﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

namespace Maelstrom.EventSystems
{

    // The EventManager is a singleton helper class that manages game events
    // It is automatically instantiated in any scene in the project
    public class EventManager : MonoBehaviour
    {

        #region Variables
        private Dictionary<string, UnityEvent> eventDictionary;
        private static EventManager eventManager;
        #endregion

        public static EventManager Instance
        {
            get
            {
                if (!eventManager)
                {
                    eventManager = FindObjectOfType<EventManager>();
                    if (!eventManager)
                    {
                        GameObject newEventManager = new GameObject("Event Manager");
                        newEventManager.AddComponent<EventManager>();
                        eventManager = newEventManager.GetComponent<EventManager>();
                    }

                    eventManager.Init();
                }
                return eventManager;
            }
        }

        #region Methods
        void Init()
        {
            if (eventDictionary == null)
                eventDictionary = new Dictionary<string, UnityEvent>();
        }

        public static void StartListening(string eventName, UnityAction listener)
        {
            UnityEvent thisEvent = null;
            if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
                thisEvent.AddListener(listener);
            else
            {
                thisEvent = new UnityEvent();
                thisEvent.AddListener(listener);
                Instance.eventDictionary.Add(eventName, thisEvent);
            }
        }

        public static void StopListening(string eventName, UnityAction listener)
        {
            if (eventManager == null)
                return;
            UnityEvent thisEvent = null;
            if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
                thisEvent.RemoveListener(listener);
        }

        public static void TriggerEvent(string eventName)
        {
            UnityEvent thisEvent = null;
            if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
                thisEvent.Invoke();
        }


        #endregion
    }
}