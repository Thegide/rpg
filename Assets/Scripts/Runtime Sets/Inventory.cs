﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maelstrom.RPG.UI;
using System.Linq;

namespace Maelstrom.RPG
{
    [CreateAssetMenu(menuName = "RPG/Inventory")]
    public class Inventory : RuntimeSet<Item>
    {

        //overflow after 2,147,483,647
        [SerializeField]
        private int m_gold = 0;
        public int Gold { get { return m_gold; } set { m_gold = value; }}

        //stackable items are limited to one stack
        public new bool Add(Item item)
        {   
            //at least one item already in inventory
            if (items.Contains(item))
            {
                if (item.IsUnique)
                {
                    UI_TextLog.instance.Log("Cannot carry anymore of that item");
                    return false;
                }
                else if (item.IsStackable && (GetCount(item) >= item.StackSize))
                { 
                    UI_TextLog.instance.Log("Cannot carry anymore of that item");
                    return false;
                }
                else
                {
                    items.Add(item);
                }
            }

            //item not in inventory
            else
            {                
                items.Add(item);
            }

            return true;
        }

        public new bool Remove (Item item)
        {
            if (items.Contains(item))
            {                               
                items.Remove(item);
                return true;
            }
            return false;
        }

        public int GetCount(Item item) 
        {
            int count = 0;

            if (items.Contains(item))
            {
                foreach (Item i in items)
                {
                    if (item == i) { count++; }
                }
            }
            return count;
        }

        public Item FindItem(string str)
        {
            Item i = items.Find(item => item.Name == str);

            return i;
        }

        public List<Item> GetBattleItems()
        {
            List<Item> battleItems = new List<Item>();

            foreach (Item item in items)
            {
                if (item.CanUseInBattle)
                {
                    battleItems.Add(item);
                }
            }

            return battleItems;
        }

        public void Sort()
        {
            items.Sort();
        }
    }
} 

