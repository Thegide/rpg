﻿using System.Collections.Generic;
using UnityEngine;

namespace Maelstrom.RPG
{    
    public abstract class RuntimeSet<T> : ScriptableObject
    {        
        [SerializeField]
        protected List<T> items = new List<T>();
        public T[] Items { get { return items.ToArray(); } }
        public int Count { get { return items.Count; } }

        public void Add(T thing)
        {
            if (!items.Contains(thing))
                items.Add(thing);
        }

        public void Remove(T thing)
        {
            if (items.Contains(thing))
                items.Remove(thing);
        }

        public bool Contains(T thing)
        {
            return items.Contains(thing);
        }
    }
}