﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maelstrom.RPG.UI;

namespace Maelstrom.RPG
{
    public class Vendor : NPC
    {
        public Inventory inventory;
        public UI_ShopWindow shopPanel;

        public override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);
            shopPanel.playerInventory = GameManager.instance.playerInventory;
            shopPanel.shopInventory = inventory;
            shopPanel.Clear();
        }

        public override void OnTriggerExit(Collider other)
        {
            shopPanel.playerInventory = null;
            shopPanel.shopInventory = null;
            shopPanel.Clear();
        }

        public override void Interact()
        {
            base.Interact();
            shopPanel.Open();
        }

        public override void Exit()
        {
            Debug.Log("Exiting shop");
            shopPanel.Close();
        }

        void UpdateShop()
        {
            shopPanel.Populate();
        }
    }
}