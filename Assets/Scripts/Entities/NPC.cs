﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maelstrom.RPG
{

    public class NPC : Interactable
    {
        #region Variables
        [SerializeField]
        protected string m_name;
        [SerializeField]
        protected string m_title;

        Animator anim;

        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        public string Title
        {
            get { return m_title; }
            set { m_title = value; }
        }
        #endregion

        public override void Awake()
        {
            base.Awake();
            anim = GetComponentInChildren<Animator>();
        }

    }
}