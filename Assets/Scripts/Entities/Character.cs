﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maelstrom.EventSystems;
using Maelstrom.UI;

namespace Maelstrom.RPG
{
    public class Character : MonoBehaviour
    {
        #region Variables

        [Header("Prefabs")]
        public GameObject DamageTextPrefab;

        [Header("Character Info")]
        [SerializeField]
        protected string m_name;
        [SerializeField]
        protected Stats m_stats;
        [SerializeField]
        protected int m_hp, m_mp;
        [HideInInspector]
        public bool IsKO = false;

        //Properties
        public Stats Stats { get { return m_stats; } set { m_stats = value; } }
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public int Level {
            get { return m_stats.level; }
            set { m_stats.level = value; }
        }
        public int Experience
        {
            get { return m_stats.experience; }
            set { m_stats.experience = value; }
        }
        public int HP
        {
            get { return this.m_hp; }
            set { this.m_hp = value; }
        }
        public int MP
        {
            get { return this.m_mp; }
            set { this.m_mp = value; }
        }
        public int MaxHP
        {
            get { return this.m_stats.maxHP; }
            set { this.m_stats.maxHP = value; }
        }
        public int MaxMP
        {
            get { return this.m_stats.maxMP; }
            set { this.m_stats.maxMP = value; }
        }
        public float HPP { get { return (float)m_hp / (float)m_stats.maxHP; } } //HP percent
        public float MPP { get { return (float)m_mp / (float)m_stats.maxMP; } } //MP percent
        public int Atk
        {
            get { return m_stats.atk; }
            set { m_stats.atk = value; }
        }
        public int MAtk
        {
            get { return m_stats.matk; }
            set { m_stats.matk = value; }
        }
        public int Def
        {
            get { return m_stats.def; }
            set { m_stats.def = value; }
        }
        public int MDef
        {
            get { return m_stats.mdef; }
            set { m_stats.mdef = value; }
        }
        public float Speed { get { return m_stats.speed; } }
        public float Evasion { get { return m_stats.evasion; } }

        public Action[] Actions
        {
            get { return m_stats.Actions; }
        }

        protected Canvas mainCanvas;
        protected Camera mainCamera;
        protected IState currentState;
        #endregion

        #region Main Methods

        protected virtual void Awake()
        {
            mainCanvas = FindObjectOfType<Canvas>();
            if (!mainCanvas)
                Debug.LogError("Error. No Canvas found in scene!");

            mainCamera = FindObjectOfType<Camera>();
            if (!mainCamera)
                Debug.LogError("Error. No Camera found in scene!");
        }

        protected void Start()
        {
            m_hp = m_stats.maxHP;
            m_mp = m_stats.maxMP;
        }

        #endregion

        #region Helper Methods

        public void ApplyDamage(DamageObject damage)
        {
            if (damage.Missed)
            {
                CreateDamageText("Miss", Color.grey);
            }
            else if (damage.hpDamage > 0)
            {
                CreateDamageText(damage.hpDamage.ToString(), Color.red);
            }
            else if (damage.hpDamage < 0)
            {
                CreateDamageText(Mathf.Abs(damage.hpDamage).ToString(), Color.green);
            }
            else
            {
                CreateDamageText(damage.hpDamage.ToString(), Color.grey);
            }
            m_hp -= damage.hpDamage;
            m_hp = Mathf.Clamp(m_hp, 0, m_stats.maxHP);

            if (m_hp == 0)
            {
                StartCoroutine(Death());
                IsKO = true;
                return;
            }
            else if (damage.hpDamage > 0)
            {
                GetHit();
            }
            else
            {
                //implement recovery animation here
            }
        }

        public void CreateDamageText(string damage, Color color)
        {
            Vector3 position = gameObject.transform.position;
            position.y += 2;

            //GameObject dmgText = Instantiate(DamageTextPrefab, mainCamera.WorldToScreenPoint(position), Quaternion.identity);
            GameObject dmgText = Instantiate(DamageTextPrefab);
            dmgText.gameObject.transform.SetParent(mainCanvas.transform, false);
            dmgText.transform.position = mainCamera.WorldToScreenPoint(position);

            dmgText.GetComponentInChildren<Text>().text = damage;
            GradientEffect gradient = dmgText.GetComponentInChildren<GradientEffect>();
            gradient.topColor = color;

            //set the bottom color to be 50% darker than the top
            float h, s, v;
            Color.RGBToHSV(gradient.topColor, out h, out s, out v);
            gradient.bottomColor = Color.HSVToRGB(h, s, v * 0.5f);
        }

        public Character[] GetTargets(TargetType set)  //could also pass in targetgroup
        {
            TargetSet targetSet = new TargetSet(this);
            List<Character> targets = new List<Character>();

            switch (set)
            {
                case (TargetType.SELF):
                    targets.Add(this);
                    break;
                case (TargetType.ALLY):
                    targets = targetSet.GetFriendlyTargets(true);
                    break;
                case (TargetType.ENEMY):
                    targets = targetSet.GetHostileTargets(true);
                    break;
                case (TargetType.ANY):
                    targets = targetSet.GetAllTargets(true);
                    break;
                case (TargetType.KO):
                    targets = targetSet.GetFriendlyTargets(false);
                    break;
                default:
                    break;
            }

            return targets.ToArray();
        }

        #endregion

        #region Animation Coroutines

        public virtual IEnumerator Attack()
        {
            // AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
            yield return new WaitForSeconds(3);
            EndTurn();
        }

        public virtual IEnumerator Cast()
        {
            yield return new WaitForSeconds(3);
            EndTurn();
        }

        public virtual IEnumerator Death()
        {
            yield return new WaitForSeconds(5); //hardcoded death animation time... replace eventually
            gameObject.SetActive(false);
        }

        public virtual void GetHit()
        {
        }

        protected void EndTurn()
        {
            EventManager.TriggerEvent("UnlockQueue");  //this to move back into State if possible... to keep lock and unlock together
            //Debug.Log(this.Name + " unlocked queue");
        }
        #endregion

    }

}