﻿using System.Collections;
using UnityEngine;
using Maelstrom.UI;
using Maelstrom.RPG.UI;

namespace Maelstrom.RPG
{
    public class Player : Character
    {
        public SpellBook spellBook;
        public Inventory inventory;
        public Equipment equipment;

        [Header("Menu Prefabs")]

        public UI_MenuManager Menus;

        #region Main Methods
        protected override void Awake()
        {
            base.Awake();
            gameObject.tag = "Player";
            RecalculateStats();
        }

        
        public void RecalculateStats()
        {

            if (equipment != null)
            {
                if (equipment.weapon != null)
                {
                    Atk = Stats.strength + equipment.weapon.attack + Level;
                    MAtk = Stats.intelligence + equipment.weapon.magicAttack + Level;
                }
                else
                {
                    Atk = Stats.strength + Level;
                    MAtk = Stats.intelligence + Level;
                }

                if (equipment.armor != null)
                {
                    Def = Stats.vitality + equipment.armor.defense + Level;
                    MDef = Stats.spirit + equipment.armor.magicDefense + Level;
                }
                else
                {
                    Def = Stats.vitality + Level;
                    MDef = Stats.spirit + Level;
                }
            }
            else
                Debug.LogError("No equipment assigned in the inspector!");
        }
        #endregion

        #region Coroutines      

        public override IEnumerator Attack()
        {
            PlayerAnimationController controller = GetComponentInChildren<PlayerAnimationController>();
            controller.Attack(2);
            yield return new WaitForSeconds(3);
            EndTurn();
        }

        public override IEnumerator Cast()
        {
            PlayerAnimationController controller = GetComponentInChildren<PlayerAnimationController>();
            controller.Cast(2);
            yield return new WaitForSeconds(3);
            EndTurn();
        }

        public override void GetHit()
        {
            PlayerAnimationController controller = GetComponentInChildren<PlayerAnimationController>();
            StartCoroutine(controller.GetHit());
        }

        public override IEnumerator Death()
        {
            PlayerAnimationController controller = GetComponentInChildren<PlayerAnimationController>();
            StartCoroutine(controller.Death());

            yield return new WaitForSeconds(5); //hardcoded death animation time... replace eventually
            gameObject.SetActive(false);
        }

        #endregion
        
        public void GainExperience(int exp)
        {
            Experience += exp;
            int expNeeded = GameLogic.ExpToNextLevel(Level);

            if (Experience >= expNeeded)
            {
                LevelUp();
                expNeeded = GameLogic.ExpToNextLevel(Level);                
            }
        }

        private void LevelUp()
        {
            Level++;
            //do level up stuff... effects/animations etc.
        }

    }
}