﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maelstrom.RPG
{
    public class Enemy : Character
    {
        public DropTable Drops;
        
        #region Main Methods
        protected override void Awake()
        {
            base.Awake();
            gameObject.tag = "Enemy";
        }

        #endregion

        #region Coroutines

        public override IEnumerator Attack()
        {
            EnemyAnimationController controller = GetComponentInChildren<EnemyAnimationController>();
            controller.Attack(2);
            yield return new WaitForSeconds(3);
            EndTurn();
        }

        public override IEnumerator Cast()
        {
            EnemyAnimationController controller = GetComponentInChildren<EnemyAnimationController>();
            controller.Cast(2);
            yield return new WaitForSeconds(3);
            EndTurn();
        }

        public override void GetHit()
        {
            EnemyAnimationController controller = GetComponentInChildren<EnemyAnimationController>();
            StartCoroutine(controller.GetHit());
        }

        public override IEnumerator Death()
        {
            EnemyAnimationController controller = GetComponentInChildren<EnemyAnimationController>();
            StartCoroutine(controller.Death());

            //if (BattleManager.instance)
            //{
            //    foreach (Player player in BattleManager.instance.players)
            //    {
            //        player.GainExperience(Experience);
            //    }
            //}

            yield return new WaitForSeconds(5); //hardcoded death animation time... replace eventually
            gameObject.SetActive(false);
        }

        #endregion
    }
}

