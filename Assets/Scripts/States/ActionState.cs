﻿using UnityEngine;
using Maelstrom.EventSystems;

namespace Maelstrom.RPG
{

    class ActionState : IState
    {
        private BattleController bc;
        private Character npc;

        private Action action;
        private Item item;
        private Character target;

        //constructor for action to take where target is specified
        public ActionState(Action action, Character target)
        {
            this.action = action;
            this.target = target;
            item = null;
        }

        public ActionState(Item item, Character target)
        {
            this.target = target;
            this.item = item;
            action = null;
        }

        public void Enter(BattleController bc)
        {
            this.bc = bc;
            npc = bc.NPC;
        }
    
        public void Exit() { }

        public void Update()
        {
            if (npc.IsKO)
            {
                bc.ChangeState(new DeathState());
                return;
            }

            if (action != null && target != null)
            {
                Debug.Log(npc.Name + ":" + action.Name + "->" + target.Name);
                action.DoAction(npc, target); //for player-chosen targets                        
            }
            else if (item != null && target != null)
            {
                Player player = npc as Player;

                if (player.inventory.Contains(item))
                {
                    item.Use(target);
                    Debug.Log(player.Name + ":" + item.Name + "->" + target.Name);
                    player.inventory.Remove(item);
                }
                player.StartCoroutine(player.Attack());  //placeholder animation
            }
            else
            {
                Debug.LogError("Something went wrong. A null parameter was passed to ActionState()");
            }

            bc.ChangeState(new IdleState());
        }
    }
}