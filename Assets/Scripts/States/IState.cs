﻿using Maelstrom.RPG;

public interface IState {

    //prepare the state
    void Enter(BattleController npc);

    void Update();

    void Exit();
}
