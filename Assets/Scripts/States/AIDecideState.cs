﻿using UnityEngine;
using UnityEngine.Events;
using Maelstrom.EventSystems;


namespace Maelstrom.RPG
{
    //AI decision state where the action to take is calculated
    class AIDecideState : IState
    {
        private BattleController bc;
        private Character npc;

        public void Enter(BattleController bc)
        {
            this.bc = bc;
            npc = bc.NPC;
        }

        public void Exit() { }      

        public void Update()
        {
            Action action = null;

            if (npc.IsKO)
            {
                bc.ChangeState(new DeathState());
                return;
            } 

            //do action that matches decision
            //for debug, first action with a match in the loop below is fired
            foreach (Action a in npc.Actions)
            {
                if (a.name == "Heal")
                {
                    action = a;
                    break;
                }

                else if (a.name == "Attack")
                    action = a;

            }

            Character[] targets = npc.GetTargets(action.TargetSet);

            if (!action.CanHaveMultipleTargets)
            {                
                if (targets.Length > 1) //picks a random target from the entity's party
                {
                    Character[] singleTarget = new Character[1];
                    int rand = Random.Range(0, targets.Length);
                    singleTarget[0] = targets[rand];
                    targets = singleTarget;
                }                
            }

            bc.ChangeState(new ActionState(action, targets[0])); //single target only atm
        }
    }
}
