﻿using UnityEngine;
using Maelstrom.EventSystems;

namespace Maelstrom.RPG
{
    class IdleState : IState
    {
        private BattleController bc;
        private Character npc;

        public void Enter(BattleController bc)
        {
            this.bc = bc;
            npc = bc.NPC;
        }

        public void Exit()
        {
            bc.TimeToNextAction = bc.MaxTimeToNextAction;
        }

        public void Update()
        {
            if (npc.IsKO)
            {
                bc.ChangeState(new DeathState());
                return;
            }

            //if there is a battle manager in the scene, process the queue
            if (BattleManager.instance)
            {
                //if action is already queued... remain idle until it is this NPC's turn
                if (BattleManager.instance.turnQueue.Contains(npc))
                {
                    if (BattleManager.instance.turnQueue.IndexOf(npc) == 0 && !BattleManager.instance.QueueIsLocked)
                    {
                        if (BattleManager.instance.battleState == BattleManager.BattleState.ONGOING)
                        {

                            BattleManager.instance.RemoveFromQueue(npc);
                            EventManager.TriggerEvent("LockQueue");
                            //Debug.Log(npc.Name + " locked queue");

                            if (npc.GetType() == typeof(Enemy))
                            {
                                bc.ChangeState(new AIDecideState());
                            }
                            else if (npc.GetType() == typeof(Player))
                            {
                                bc.ChangeState(new PlayerDecideState());
                            }
                        }
                    }
                }
            }

            if (bc.TimeToNextAction > 0)
            {
                bc.TimeToNextAction -= npc.Speed * Time.deltaTime;
            }
            else
            {
                if (BattleManager.instance)
                {
                    BattleManager.instance.AddToQueue(npc);
                }
                else
                {
                    bc.ChangeState(new PlayerDecideState());
                }
            }
        }
    }
}