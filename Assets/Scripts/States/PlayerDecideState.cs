﻿using UnityEngine;
using UnityEngine.Events;
using Maelstrom.EventSystems;


namespace Maelstrom.RPG
{
    //Player decision state where the action to take is selected from a menu
    class PlayerDecideState : IState
    {
        private BattleController bc;     
        private Player player;

        private Action action;
        private UnityAction playerInputListener;

        public void Enter(BattleController bc)
        {
            this.bc = bc;
            player = (Player)bc.NPC;

            //open Command menu and initialize it to the player's actions
            player.Menus.SwitchMenus(player.Menus.Commands);
            player.Menus.Commands.Initialize();
        }

        public void Exit() { }        

        public void Update()
        {
            if (player.IsKO)
            {
                bc.ChangeState(new DeathState());
                return;
            }

            //player.ChangeState(new ActionState(action));  //called by UI_CommandMenu
        }
    }   
}
