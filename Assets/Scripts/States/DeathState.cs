﻿#pragma warning disable 414 //warning about bc not being used

namespace Maelstrom.RPG
{
    class DeathState : IState
    {
        private BattleController bc;
        private Character npc;

        public void Enter(BattleController bc)
        {
            this.bc = bc;
            npc = bc.NPC;
                        
            //Debug.Log(npc.Name + " is dead");

            //remove any remaining queued actions
            if (BattleManager.instance.turnQueue.Contains(npc))
                BattleManager.instance.RemoveFromQueue(npc);
        }

        public void Exit() { }

        public void Update()
        {
            //if revive...  bc.ChangeState(new IdleState());
        }
    }
}