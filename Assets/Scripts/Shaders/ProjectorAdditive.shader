Shader "Projector/Additive" {
	Properties {
		_Color ("Tint Color", Color) = (1,1,1,1)
		_MainTex ("Cookie", 2D) = "" {}
	}
	
	Subshader {
		Tags {"Queue"="Transparent"}
		Pass {
			ZWrite Off
			ColorMask RGB
			Blend SrcAlpha One
			Offset -1, -1

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			fixed4 _Color;
			sampler2D _MainTex;

			struct v2f {
				float4 uvShadow : TEXCOORD0;
				float4 pos : SV_POSITION;
			};
			
			float4x4 unity_Projector;
			float4x4 unity_ProjectorClip;
			
			v2f vert (float4 vertex : POSITION)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(vertex);
				o.uvShadow = mul (unity_Projector, vertex);
				return o;
			}
			
			fixed4 frag(v2f i) : SV_Target
			{
				// Apply alpha mask
				fixed4 texCookie = tex2Dproj(_MainTex, UNITY_PROJ_COORD(i.uvShadow));
				texCookie.rgb = _Color * texCookie.a;
				return texCookie;
			}
			ENDCG
		}
	}
}
