﻿using UnityEngine;
using UnityEngine.UI;
using Maelstrom.UI;

namespace Maelstrom.RPG.UI
{

    public class UI_MainMenuManager : MonoBehaviour
    {
        
        public Selectable defaultSelectable;

        public void LoadMenu()
        {
            Debug.Log("Loading Menu");
        }


        public void QuitGame()
        {
            Debug.Log("Quitting Game.  Goodbye");
            Application.Quit();
        }

        public void SetDefaultSelectable()
        {
            defaultSelectable.Select();
        }

        public void Select(Selectable select)
        {
            select.Select();
        }
    }
}