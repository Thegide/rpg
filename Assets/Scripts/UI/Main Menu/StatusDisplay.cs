﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using Maelstrom.UI;

namespace Maelstrom.RPG.UI
{

    public class StatusDisplay : MonoBehaviour
    {
        #region Variables

        public Player player;
        private Stats stats;

        [Header("Components")]
        public Text nameText;
        public Text levelText;
        public Text HPText;
        public Text strText, vitText, intText, sprText;
        public Text atkText, defText, matkText, mdefText;
        public ExperienceBar xpBar;

        public Image image;

        #endregion

        void Start()
        {
            Initialize();
        }

        public void Initialize()
        {
            if (player)
            {

                stats = player.Stats;
                player.RecalculateStats();

                xpBar = gameObject.GetComponentInChildren<ExperienceBar>();
                if (xpBar)
                {
                    xpBar.player = stats;
                }
            }
        }

        
        void Update()
        {
            nameText.text = player.Name;
            levelText.text = "Lv. " + player.Level.ToString();
            HPText.text = player.HP + "/" + player.MaxHP;
            image.sprite = stats.sprite;

            strText.text = "STR: " + stats.strength.ToString();
            vitText.text = "VIT: " + stats.vitality.ToString();
            intText.text = "INT: " + stats.intelligence.ToString();
            sprText.text = "SPR: " + stats.spirit.ToString();

            atkText.text = "ATK: " + stats.atk.ToString();
            defText.text = "DEF: " + stats.def.ToString();
            matkText.text = "M.ATK: " + stats.matk.ToString();
            mdefText.text = "M.DEF: " + stats.mdef.ToString();
        }
    }
}