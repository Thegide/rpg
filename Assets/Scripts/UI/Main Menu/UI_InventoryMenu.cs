﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Linq;
using Maelstrom.UI;

namespace Maelstrom.RPG.UI
{
    public class UI_InventoryMenu : UI_Window
    {
        [SerializeField]
        private Inventory inventory;
        private List<Item> uniqueItemList;

        [SerializeField]
        private GameObject displayItemArea;
        public GameObject rowItemPrefab;

        [HideInInspector]
        private UnityEvent listener; // listener for onInventoryChanged

        #region Main Methods

        new void Update()
        {
            base.Update();

            if (Input.GetButtonDown("Y"))
            {
                Sort();
                Initialize();
            }

        }

        public void Initialize()
        {
            if (inventory == null)
            {
                inventory = GameManager.instance.playerInventory;
            }

            BuildInventory();
            Clear();

            foreach (Item item in uniqueItemList)
            {
                GameObject rowItem = Instantiate(rowItemPrefab) as GameObject;
                rowItem.transform.SetParent(displayItemArea.transform, false);

                UI_InventoryButton itemButton = rowItem.GetComponent<UI_InventoryButton>();
                if (itemButton)
                {
                    itemButton.Text.text = item.Name;
                    itemButton.Quantity.text = inventory.GetCount(item).ToString();
                    itemButton.Icon.sprite = item.Icon;
                    itemButton.Item = item;
                }
            }
        }

        #endregion
   

        #region Helper Methods
        void BuildInventory()
        {
            uniqueItemList = new List<Item>();
            foreach (Item item in inventory.Items)
            {
                if (!uniqueItemList.Contains(item))
                {
                    uniqueItemList.Add(item);
                }
            }
        }

        void Sort()
        {
            inventory.Sort();
        }

        void Clear()
        {
            UI_InventoryButton[] linesToRemove;
            linesToRemove = gameObject.GetComponentsInChildren<UI_InventoryButton>();

            foreach (UI_InventoryButton i in linesToRemove)
                Destroy(i.gameObject);
        }


        #endregion

    }
}
