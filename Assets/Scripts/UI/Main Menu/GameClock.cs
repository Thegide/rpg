﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Maelstrom.UI
{
    public class GameClock : MonoBehaviour
    {
        public float time;
        private Text timerText;

        private TimeSpan elapsedTime;     

        // Use this for initialization
        void Start()
        {
            timerText = GetComponentInChildren<Text>();
        }

        // Update is called once per frame
        void Update()
        {

            time += Time.deltaTime;
            elapsedTime = TimeSpan.FromSeconds(time);

            string displayTime = string.Format("{0:D}:{1:D2}:{2:D2}",
                elapsedTime.Days * 24 + elapsedTime.Hours,
                elapsedTime.Minutes,
                elapsedTime.Seconds);

            // .NET4.5 and up
            //string displayTime = elapsedTime.ToString(@"hh\:mm\:ss");

            timerText.text = displayTime;
        }

        public void SwitchMenus()
        {


        }
    }
}