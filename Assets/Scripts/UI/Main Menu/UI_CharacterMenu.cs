﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maelstrom.UI;

namespace Maelstrom.RPG.UI
{
    public class UI_CharacterMenu : UI_Window
    {
        [SerializeField]
        private UI_Slider sliderScript;
        public GameObject sliderPrefab;

        public List<Player> players;

        // Use this for initialization
        void Start()
        {
            sliderScript = GetComponentInChildren<UI_Slider>();

            if (GameManager.instance && sliderScript != null)
            {
                players = GameManager.instance.party;

                foreach (Player p in players)
                {
                    GameObject playerPanel = Instantiate(sliderPrefab, sliderScript.Container.transform);

                    StatusDisplay statsScript = playerPanel.GetComponentInChildren<StatusDisplay>();
                    EquipmentSelector equipScript = playerPanel.GetComponentInChildren<EquipmentSelector>();
                    if (statsScript)
                    {
                        statsScript.player = p;
                        statsScript.Initialize();
                    }

                    if (equipScript)
                    {
                        equipScript.player = p;
                    }
                }

                sliderScript.Initialize();
            }
        }

        public new void Update()
        {
            base.Update();

            if(inputTriggerLeft && HasFocus)
            {
                sliderScript.prevButton.GetComponent<Button>().onClick.Invoke();
            }
            if (inputTriggerRight && HasFocus)
            {
                sliderScript.nextButton.GetComponent<Button>().onClick.Invoke();
            }


        }

    }
}