﻿using UnityEngine;
using UnityEngine.UI;

namespace Maelstrom.RPG.UI
{

    public class ItemInfoDisplay : MonoBehaviour
    {

        public Text nameText;
        public Text descriptionText;
        public Image image;

        void Start()
        {
            nameText.text = "";
            descriptionText.text = "";
            image.color = new Color(1, 1, 1, 0);
        }

        public void DisplayItemInfo(Item item)
        {
            if (item != null)
            {
                nameText.text = item.Name;
                descriptionText.text = item.Description;
                image.color = new Color(1, 1, 1, 1);
                image.sprite = item.Icon;
            }

            else
            {
                Start();
            }
        }
    }
}