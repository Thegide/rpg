﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maelstrom.RPG.UI
{
    public class EquipmentSelector : MonoBehaviour
    {
        #region Variables

        public Player player;

        [Header("Components")]
        public Dropdown weaponSelector;
        public Dropdown armorSelector;
        public Dropdown accessorySelector;

        private List<Weapon> weapons;
        private List<Armor> armors;
        private List<Accessory> accessories;

        #endregion

        void Start()
        {
            weaponSelector.onValueChanged.AddListener(delegate { DropDownValueChanged(weaponSelector); });
            armorSelector.onValueChanged.AddListener(delegate { DropDownValueChanged(armorSelector); });
            accessorySelector.onValueChanged.AddListener(delegate { DropDownValueChanged(accessorySelector); });

            Initialize();
        }

       
        void Update()
        {
            //show...hide... 
            //each show() should re-call initialize();
        }

        #region Helper Methods

        void Initialize()
        {
            //reset the dropdowns
            weaponSelector.ClearOptions();
            armorSelector.ClearOptions();
            accessorySelector.ClearOptions();

            if (player)
            {
                //populate dropdowns with equippable items from inventory
                weaponSelector.AddOptions(GetWeapons());
                armorSelector.AddOptions(GetArmors());
                accessorySelector.AddOptions(GetAccessories());

                //select currently equipped items
                weaponSelector.value = GetIndexOfEquippedItem(weaponSelector);
                armorSelector.value = GetIndexOfEquippedItem(armorSelector);
                accessorySelector.value = GetIndexOfEquippedItem(accessorySelector);
                //weaponSelector.RefreshShownValue();
            }
            else
            {
                Debug.LogError("No player stats found.  Did you forget to assign it in the inspector?");
            }
        }

        //delegate function that assigns new gear to the player based on dropdown selection
        void DropDownValueChanged(Dropdown change)
        {
            string match = change.captionText.text;

            if (change.Equals(weaponSelector))
            {
                player.equipment.weapon = player.inventory.FindItem(match) as Weapon;
                player.RecalculateStats();
            }
            else if (change.Equals(armorSelector))
            {
                player.equipment.armor = player.inventory.FindItem(match) as Armor;
                player.RecalculateStats();
            }
            else if (change.Equals(accessorySelector))
            {
                player.equipment.accessory = player.inventory.FindItem(match) as Accessory;
                player.RecalculateStats();
            }
        }

        //finds the index of the currently equipped item in Dropdown.Options
        int GetIndexOfEquippedItem(Dropdown dropdown)
        {
            if (dropdown.Equals(weaponSelector))
            {
                if (player.equipment.weapon == null)
                    return 0;
                for (int i = 0; i < dropdown.options.Count; i++)
                {
                    if (dropdown.options[i].text == player.equipment.weapon.Name)
                    {
                        return i;
                    }
                }
            }
            else if (dropdown.Equals(armorSelector))
            {
                if (player.equipment.armor == null)
                    return 0;
                for (int i = 0; i < dropdown.options.Count; i++)
                {
                    if (dropdown.options[i].text == player.equipment.armor.Name)
                    {
                        return i;
                    }
                }

            }

            else if (dropdown.Equals(accessorySelector))
            {
                if (player.equipment.accessory == null)
                    return 0;
                for (int i = 0; i < dropdown.options.Count; i++)
                {
                    if (dropdown.options[i].text == player.equipment.accessory.Name)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        List<string> GetWeapons()
        {
            List<string> weapons = new List<string>();
            weapons.Add("None");

            Inventory inv = player.inventory;
            foreach (Item item in inv.Items)
            {
                if (item.GetType() == typeof (Weapon))
                {
                    weapons.Add(item.Name);                    
                }
            }            
            return weapons;
        }

        List<string> GetArmors()
        {
            List<string> armors = new List<string>();
            armors.Add("None");

            Inventory inv = player.inventory;
            foreach (Item item in inv.Items)
            {
                if (item.GetType() == typeof(Armor))
                {
                    armors.Add(item.Name);
                }
            }

            return armors;
        }

        List<string> GetAccessories()
        {
            List<string> accessories = new List<string>();
            accessories.Add("None");

            Inventory inv = player.inventory;
            foreach (Item item in inv.Items)
            {
                if (item.GetType() == typeof(Accessory))
                {
                    accessories.Add(item.Name);                    
                }
            }

            return accessories;
        }

        #endregion

    }
}