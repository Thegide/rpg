﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Maelstrom.RPG.UI {

    public class UI_InventoryButton : MonoBehaviour, ISelectHandler, IDeselectHandler {

        public Image Icon;
        public Text Text;
        public Text Quantity;

        [HideInInspector]
        public Item Item;

        public void OnSelect(BaseEventData eventData)
        {
            UI_InventoryMenu menuScript = GetComponentInParent<UI_InventoryMenu>();
            if (menuScript)
            {
                ItemInfoDisplay itemData = menuScript.GetComponentInChildren<ItemInfoDisplay>(true);
                if (itemData)
                {
                    itemData.DisplayItemInfo(Item);
                }

            }
        }

        public void OnDeselect(BaseEventData eventData)
        {
            UI_InventoryMenu menuScript = GetComponentInParent<UI_InventoryMenu>();
            if (menuScript)
            {
                ItemInfoDisplay itemData = menuScript.GetComponentInChildren<ItemInfoDisplay>();
                if (itemData)
                {
                    itemData.DisplayItemInfo(null);
                }
            }
        }
    }
}