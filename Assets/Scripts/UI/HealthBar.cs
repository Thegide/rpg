﻿using UnityEngine;
using UnityEngine.UI;
using Maelstrom.RPG;

namespace Maelstrom.UI {

    public class HealthBar : MonoBehaviour {

        [Header("Entity Data")]
        public Character entity;

        [Header("Components")]
        public Text nameText;
        public Text HPText, MPText;
        public StatBar HPBar, MPBar;

        #region Main Methods
        void Start()
        {
            if (!entity)
                Debug.LogError("Entity not found.  Did you forget to attach it?");
        }

        void Update()
        {
            nameText.text = entity.name;
            HPText.text = entity.HP + "/" + entity.MaxHP;
            MPText.text = entity.MP + "/" + entity.MaxMP;

            HPBar.Percentage = entity.HPP;
            MPBar.Percentage = entity.MPP;

            nameText.color = SetColor(entity.HPP);
            HPText.color = SetColor(entity.HPP);
            MPText.color = SetColor(entity.MPP);            
        }
        #endregion

        public Color SetColor(float value)
        {
            if (value <= 0)
                return Color.red;
            else if (value < 0.25f)
                return Color.yellow;
            else
                return Color.white;
        }
    }    
}