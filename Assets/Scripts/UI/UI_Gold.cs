﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Maelstrom.RPG.UI
{

    public class UI_Gold : MonoBehaviour
    {

        public Inventory Inventory;
        public Text GoldText;

        // Update is called once per frame
        void Update()
        {
            SetGold();
        }

        void SetGold()
        {
            if (Inventory)
            {
                GoldText.text = String.Format("{0:n0}", Inventory.Gold);
            }
            else
                GoldText.text = "0";
        }

    }
}