﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Maelstrom.UI;
using Maelstrom.EventSystems;

namespace Maelstrom.RPG.UI
{
    public class UI_ItemMenu : UI_DynamicMenu
    {
        #region Variables

        [Header("Item Menu Prefs")]
        public Text titleText;
        private Player player;

        #endregion

        public void Initialize()
        {
            player = GetComponentInParent<Player>();
            if (!player)
            {
                Debug.LogError("Cannot instantiate " + GetType().ToString() + " No player assigned");
            }
            else
            {
                if (!player.inventory)
                {
                    Debug.LogError("No inventory found.  Did you forget to add it in the inspector?");
                    return;
                }
            }
            
            Item[] items = player.inventory.GetBattleItems().ToArray();
           
            string[] labels = new string[items.Length];
            for (int i = 0; i < items.Length; i++)
            {
                labels[i] = items[i].Name;
            }
            Populate(labels);
        }

        public override void DoButtonAction(string name)
        {
            Item item = player.inventory.FindItem(name);

            if (item)
            {
                player.Menus.SwitchMenus(player.Menus.Targets);
                player.Menus.Targets.Initialize(item);
                player.Menus.Targets.SetParentMenu(this);
            } 
            else
            {
                Debug.LogError(name + " not found in inventory");
            }
        }
    }
}