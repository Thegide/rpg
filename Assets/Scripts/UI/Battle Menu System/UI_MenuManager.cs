﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Maelstrom.RPG.UI;
using Maelstrom.RPG;

namespace Maelstrom.UI
{
    public class UI_MenuManager : MonoBehaviour
    {
        public UnityEvent onSwitchedMenus = new UnityEvent();
        public UnityEvent onCancelMenu = new UnityEvent();

        private UI_DynamicMenu lastActiveMenu;
        public UI_DynamicMenu LastActiveMenu { get { return lastActiveMenu; } }

        private UI_DynamicMenu currentActiveMenu;
        public UI_DynamicMenu CurrentActiveMenu { get { return currentActiveMenu; } }

        //RPG Menu definitions
        [HideInInspector]
        public UI_CommandMenu Commands;
        [HideInInspector]
        public UI_ItemMenu Items;
        [HideInInspector]
        public UI_SpellMenu Spells;
        [HideInInspector]
        public UI_TargetMenu Targets;

        private void Start()
        {
            Commands = GetComponentInChildren<UI_CommandMenu>();
            Items = GetComponentInChildren<UI_ItemMenu>();
            Spells = GetComponentInChildren<UI_SpellMenu>();
            Targets = GetComponentInChildren<UI_TargetMenu>();
        }

        void Update()
        {
            if (Input.GetButtonDown("B"))
            {
                if (currentActiveMenu)
                {
                    GoToParentMenu();
                }
            }
        }

        public void SwitchMenus(UI_DynamicMenu menu)
        {
            if (menu)
            {
                if (currentActiveMenu)
                {
                    lastActiveMenu = currentActiveMenu;
                    lastActiveMenu.Disable();
                }
                currentActiveMenu = menu;
                currentActiveMenu.Enable();

                if (onSwitchedMenus != null)
                {
                    onSwitchedMenus.Invoke(); //if events need to be triggered on switch
                }

                if (currentActiveMenu.IsOpen == false)
                {
                    currentActiveMenu.Open(); //what if menu is already open?                
                }
                else {
                }
                    //reset selection
            }
        }

        public void GoToParentMenu()
        {
            if (currentActiveMenu)
            {

                if (currentActiveMenu.ParentMenu) //would be at root
                {
                    onCancelMenu.Invoke();
                    currentActiveMenu.Close();
                    currentActiveMenu.Disable();
                    SwitchMenus(currentActiveMenu.ParentMenu);
                }
            
            }
        }

    }
}