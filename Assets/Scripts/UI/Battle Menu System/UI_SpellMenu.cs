﻿using UnityEngine;
using UnityEngine.UI;
using Maelstrom.UI;

namespace Maelstrom.RPG.UI
{
    public class UI_SpellMenu : UI_DynamicMenu
    {
        #region Variables
        [Header("Command Menu Prefs")]
        public Text titleText;

        private Player player;
   
        #endregion

        public void Initialize()
        {
            player = GetComponentInParent<Player>();
            if (!player)
            {
                Debug.LogError("Cannot instantiate " + GetType().ToString() + " No player assigned");
            }

            SpellBook spellBook = player.spellBook;
            if (!spellBook)
            {
                Debug.LogError("No SpellBook found.  Did you forget to add it in the inspector?");
                return;
            }

            string[] labels = new string[spellBook.Count];
            for (int i = 0; i < spellBook.Count; i++)
            {
                labels[i] = spellBook.Spells[i].Name;
            }
            Populate(labels);
        }

        public override void DoButtonAction(string name)
        {
            Spell spell = player.spellBook.FindSpell(name);

            if (spell)
            {
                player.Menus.SwitchMenus(player.Menus.Targets);
                player.Menus.Targets.Initialize(spell);
                player.Menus.Targets.SetParentMenu(this);
            }
            else
            {
                Debug.LogError(name + " not found in inventory");
            }
        }

    }

   
}