﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using Maelstrom.UI;
using Maelstrom.EventSystems;

namespace Maelstrom.RPG.UI
{

    public class UI_TargetMenu : UI_DynamicMenu
    {
        #region Variables
        [Header("Target Menu Prefs")]
        public Text titleText;

        private Action action;
        private Item item;
        private Character[] targets = new Character[0];

        private Player player;
        #endregion

        public void Initialize(Action action)
        {
            player = GetComponentInParent<Player>();
            if (!player)
            {
                Debug.LogError("Cannot instantiate " + GetType().ToString() + " No player assigned");
            }

            if (action)
                this.action = action;
            else
                return;
                    
            titleText.text = action.Name;
            item = null; //not an item command

            if (BattleManager.instance)
            {
                targets = player.GetTargets(action.TargetSet);
            }

            string[] labels = new string[targets.Length];
            for (int i = 0; i < targets.Length; i++)
            {
                labels[i] = targets[i].Name;
            }
            Populate(labels);
        }

        public void Initialize(Item item)
        {
            player = GetComponentInParent<Player>();
            if (!player)
            {
                Debug.LogError("Cannot instantiate " + GetType().ToString() + " No player assigned");
            }

            titleText.text = item.Name;
            this.item = item;

            if (BattleManager.instance)
            {
                targets = player.GetTargets(item.TargetSet);
            }

            string[] labels = new string[targets.Length];
            for (int i = 0; i < targets.Length; i++)
            {
                labels[i] = targets[i].Name;
            }
            Populate(labels);
        }

        public override void DoButtonAction(string name) //name is name of target
        {
            Character target = null;

            for (int i = 0; i < targets.Length; i++)
            {
                if (targets[i].Name == name)
                {
                    target = targets[i];
                    break;
                }
            }

            Debug.Log("Player: " + player.Name);

            if (target != null)
            {
                BattleController bc = player.GetComponent<BattleController>();

                if (!bc)
                {
                    Debug.LogError("Cannot action command.  No battle controller attached to NPC game object");
                }

                if (item == null)
                    bc.ChangeState(new ActionState(action, target));
                else
                {
                    bc.ChangeState(new ActionState(item, target));
                }
                EventManager.TriggerEvent("CloseMenu");
            }
        }
    }
}