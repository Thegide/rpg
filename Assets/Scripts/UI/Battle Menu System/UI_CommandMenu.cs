﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Maelstrom.UI;

namespace Maelstrom.RPG.UI
{
    public class UI_CommandMenu : UI_DynamicMenu
    {
        #region Variables
        [Header("Command Menu Prefs")]
        public Text titleText;

        private Player player;
        #endregion

        public void Initialize()
        {
            player = GetComponentInParent<Player>();
            if (!player)
            {
                Debug.LogError("Cannot instantiate " + GetType().ToString() + " No player assigned");
            }

            titleText.text = player.Name;

            List<string> labels = new List<string>();
            for (int i = 0; i < player.Actions.Length; i++)
            {
                labels.Add(player.Actions[i].Name);
            }

            if (player.spellBook.Count > 0)
            {
                labels.Add("Spells");
            }

            if (player.inventory.GetBattleItems().Count > 0)
            {
                labels.Add("Items");
            }
            
            Populate(labels.ToArray());
        }

        public override void DoButtonAction(string command)
        {
            //Action action = null;

            //for (int i = 0; i < player.Actions.Length; i++)
            //{
            //    if (player.Actions[i].Name == name)
            //    {
            //        action = player.Actions[i];
            //        break;
            //    }
            //}

            //for submenus
            if (command == "Spells")
            {
                player.Menus.SwitchMenus(player.Menus.Spells);
                player.Menus.Spells.Initialize();
                player.Menus.Spells.SetParentMenu(this);
            }
            else if (command == "Items")
            {
                player.Menus.SwitchMenus(player.Menus.Items);
                player.Menus.Items.Initialize();
                player.Menus.Items.SetParentMenu(this);
            }
            else //for all direct commands that immediately pick targets
            {
                Action action = Array.Find(player.Actions, a => a.Name == command);

                if (action)
                {
                    player.Menus.SwitchMenus(player.Menus.Targets);
                    player.Menus.Targets.Initialize(action);
                    player.Menus.Targets.SetParentMenu(this);
                }
                else
                    Debug.LogError("Action " + name + " not found");
            }
        }
    }
}
