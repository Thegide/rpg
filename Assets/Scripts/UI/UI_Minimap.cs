﻿using UnityEngine;

namespace Maelstrom.RPG.UI
{   
    public class UI_Minimap : MonoBehaviour
    {
        [SerializeField]
        private Camera cam;
        public Transform target;
        public float zoom = 50;

        void Start()
        {
            if (cam == null)
            {
                Debug.LogError("No camera found attached to this object");
            }
        }

        void LateUpdate()
        {
            if (target)
            {
                Vector3 pos = target.position;
                pos.y = cam.transform.position.y;
                cam.transform.position = pos;
                cam.orthographicSize = zoom;
            }
            else
                Debug.LogWarning("Minimap needs a target");
        }
    }
}