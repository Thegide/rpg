﻿using UnityEngine;
using Maelstrom.UI;
using UnityEngine.UI;

namespace Maelstrom.RPG.UI
{
    public class UI_ShopWindow : UI_Window
    {
        public GameObject buyWindow;
        public GameObject sellWindow;

        public Inventory playerInventory;
        public Inventory shopInventory;

        public GameObject buyRowPrefab;
        public GameObject sellRowPrefab;

        public void Populate()
        {
            PopulateBuyWindow();
            PopulateSellWindow();
        }

        //Populate window for buying items from vendor
        public void PopulateBuyWindow()
        {
            ClearBuyWindow();
            if (shopInventory != null)
            {
                Button[] buttons = new Button[shopInventory.Count];

                for (int i = 0; i < shopInventory.Count; i++)
                {
                    GameObject rowItem = Instantiate(buyRowPrefab, buyWindow.transform);
                    UI_InventoryButton itemButton = rowItem.GetComponent<UI_InventoryButton>();
                    Item item = shopInventory.Items[i];

                    if (itemButton)
                    {
                        itemButton.Text.text = item.Name;
                        //itemButton.Quantity.text = shopInventory.ItemCount(item).ToString();
                        itemButton.Quantity.text = item.Cost.ToString();
                        itemButton.Icon.sprite = item.Icon;
                        itemButton.Item = item;
                    }

                    Button button = rowItem.GetComponentInChildren<Button>();
                    buttons[i] = button;
                    button.onClick.AddListener(delegate { Buy(item); });
                }
            }
        }

        //Populate window for selling items from inventory
        public void PopulateSellWindow()
        {
            ClearSellWindow();
            if (playerInventory != null)
            {
                Button[] buttons = new Button[playerInventory.Count];

                for (int i = 0; i < playerInventory.Count; i++)
                {
                    GameObject rowItem = Instantiate(sellRowPrefab, sellWindow.transform);
                    UI_InventoryButton itemButton = rowItem.GetComponent<UI_InventoryButton>();
                    Item item = playerInventory.Items[i];

                    if (itemButton)
                    {
                        itemButton.Text.text = item.Name;
                        //itemButton.Quantity.text = playerInventory.ItemCount(item).ToString();
                        itemButton.Quantity.text = (item.SellValue()).ToString();
                        itemButton.Icon.sprite = item.Icon;
                        itemButton.Item = item;
                    }

                    Button button = rowItem.GetComponentInChildren<Button>();
                    buttons[i] = button;
                    button.onClick.AddListener(delegate { Sell(item); });
                    button.onClick.AddListener(delegate { Destroy(button.gameObject); });
                }
            }
        }

        public void Clear()
        {
            ClearBuyWindow();
            ClearSellWindow();
        }

        void ClearBuyWindow()
        {
            foreach (Transform child in buyWindow.transform)
            {
                if (!child.gameObject.CompareTag("DoNotDestroy"))
                {
                    Destroy(child.gameObject);
                }
            }
        }

        void ClearSellWindow()
        {
            foreach (Transform child in sellWindow.transform)
            {
                if (!child.gameObject.CompareTag("DoNotDestroy"))
                {
                    Destroy(child.gameObject);
                }
            }
        }

        void Buy(Item item)
        {
            if (GameManager.instance.playerInventory.Gold < item.Cost)
            {
                UI_TextLog.instance.Log("You do not have enough gold to buy that");
                return;
            }

            if (shopInventory.Contains(item))
            {
                bool success = GameManager.instance.playerInventory.Add(item);
                if (success)
                {
                    GameManager.instance.playerInventory.Gold -= item.Cost;
                    UI_TextLog.instance.Log("Bought " + item.Name + " for " + item.SellValue() + " gold");
                }
            }
        }

        void Sell(Item item)
        {
            if (GameManager.instance.playerInventory.Contains(item))
            {
                bool success = GameManager.instance.playerInventory.Remove(item);
                if (success)
                {
                    GameManager.instance.playerInventory.Gold += item.SellValue();
                    UI_TextLog.instance.Log("Sold " + item.Name + " for " + item.SellValue() + " gold");
                }
            }
        }        
    }
}