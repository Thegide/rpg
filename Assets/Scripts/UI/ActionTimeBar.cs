﻿using UnityEngine;
using UnityEngine.UI;
using Maelstrom.RPG;

namespace Maelstrom.UI
{
    public class ActionTimeBar : MonoBehaviour
    {

        public BattleController bc;        
        public Image fillImage;

        private float maxTime;
        private float timeToAction;

        private void Start()
        {
            if (!bc)
                Debug.LogError("Battle controller for NPC not found.  Did you forget to attach it?");

            maxTime = bc.MaxTimeToNextAction;
            timeToAction = bc.TimeToNextAction;
        }


        void Update()
        {

            maxTime = bc.MaxTimeToNextAction;
            timeToAction = bc.TimeToNextAction;

            fillImage.fillAmount = Mathf.Clamp01(timeToAction / maxTime);
        }
    }
}