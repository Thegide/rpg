﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class FadeAndDestroy : MonoBehaviour {

    Animator anim;
    
    public float lifetime;
    public float fadeTime;

	void Start () {
        anim = GetComponent<Animator>();
        anim.speed = 1.0f / fadeTime;
	}
	
	void Update () {
        lifetime -= Time.deltaTime;
        
        if (lifetime <= 0)
        {
            anim.SetTrigger("fade");
            Destroy(gameObject, fadeTime + 1);
        }
	}
}
