﻿using UnityEngine;
using UnityEngine.UI;

public class DisplaySliderValue : MonoBehaviour {

    public void SetValue(Slider slider) {
        Text text = GetComponent<Text>();

        int value = (int)(slider.value * 100);
        text.text = value.ToString();
    }
}
