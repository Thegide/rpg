using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

[AddComponentMenu("UI/Effects/Gradient")]

//creates a 2-color vertical gradient effect

public class GradientEffect : BaseMeshEffect {

    public Color32 topColor = Color.white;
    public Color32 bottomColor = Color.black;

    public override void ModifyMesh (VertexHelper vertexHelper) {
        
        if (!this.IsActive())
            return;

        List<UIVertex> vertexList = new List<UIVertex>();

        vertexHelper.GetUIVertexStream(vertexList);

        if (vertexList.Count == 0)
            return;

        float bottomY = vertexList[0].position.y;
        float topY = vertexList[0].position.y;

        for (int i = 1; i < vertexList.Count; i++) {
            float y = vertexList[i].position.y;
            if (y > topY) {
                topY = y;
            }
            else if (y < bottomY) {
                bottomY = y;
            }
        }

        float uiElementHeight = topY - bottomY;

        for (int i = 0; i < vertexList.Count; i++) {
            UIVertex uiVertex = vertexList[i];

            uiVertex.color = Color32.Lerp(bottomColor, topColor, (uiVertex.position.y - bottomY) / uiElementHeight);

            vertexList[i] = uiVertex;
        }     

        vertexHelper.Clear();
        vertexHelper.AddUIVertexTriangleStream(vertexList);

    }
        
}
