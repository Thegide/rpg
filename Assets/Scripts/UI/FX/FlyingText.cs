﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[AddComponentMenu("UI/Effects/FlyingText")]

public class FlyingText : MonoBehaviour {

    public Text Text;
    public enum Direction {up, down};
    public Direction direction;

	// Use this for initialization
	void Start () {
        
        if (direction == Direction.up)
        {
            GetComponent<Animation>().Play("FadeUp");    // useless until I find a way to play the animations locally.  
        }
        else if (direction == Direction.down)
        {
            GetComponent<Animation>().Play("FadeDown"); 
        }
    }   
    

}
