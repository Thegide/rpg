﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maelstrom.FX
{
    [RequireComponent (typeof (Projector))]
    public class AnimatedProjector : MonoBehaviour
    {

        protected Projector projector;
        public Color color;
        public int speed = 60;

        void Start()
        {
            projector = gameObject.GetComponent<Projector>();
            gameObject.transform.parent.gameObject.layer = 10; //Entities layer
            projector.fieldOfView = 0f;
        }


        void Update()
        {
            //projector.transform.Rotate(Vector3.forward, speed * Time.deltaTime);            
            if (projector.fieldOfView < 60f)
            {
                projector.fieldOfView += speed * Time.deltaTime;
            }
        }
    }
}