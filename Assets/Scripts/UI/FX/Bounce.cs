﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce : MonoBehaviour {

    public int speed;
    public float distance;
    public Vector3 offset;

    private Vector3 localPosition;
    
    void Update ()
    {
        Vector3 bouncePos = new Vector3(0f, Mathf.Sin(Time.time * speed), 0f);

        transform.position = transform.parent.position + offset + bouncePos * distance / 10;
	}
}
