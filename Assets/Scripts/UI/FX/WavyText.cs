﻿//temporarily disable warnings for this script in development
#pragma warning disable 0219, 414

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]  //Text to animate
[RequireComponent(typeof(Animator))]  //for transitions
[RequireComponent(typeof(CanvasGroup))]  //allows to control transparency 
[RequireComponent(typeof(ContentSizeFitter))]
[RequireComponent(typeof(HorizontalLayoutGroup))]
public class WavyText : MonoBehaviour {
        
    public float rippleSize;
    public float rippleSpeed;
    public float fadeTime;

    private Text text;
    private Font font;

    private Animator animator;
    private ContentSizeFitter size;
    private char[] charArray;

    private Vector3 initialPos, currentPos, targetPos;
    private Canvas canvas;
    private RectTransform rt;

    private bool returnMotion;
    
    void Awake () {

        text = GetComponent<Text>();
        animator = GetComponent<Animator>();
        size = GetComponent<ContentSizeFitter>();
        size.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        size.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;

        charArray = text.text.ToCharArray();

        foreach (char c in charArray)
        {
            GameObject charObject = new GameObject(c.ToString());
            charObject.AddComponent<Text>();
            charObject.AddComponent<ContentSizeFitter>();
            charObject.transform.SetParent(this.transform);
            Text charText = charObject.GetComponent<Text>();
            ContentSizeFitter charSize = charObject.GetComponent<ContentSizeFitter>();
            charSize.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
            charSize.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;

            charText.font = text.font;
            charText.fontSize = text.fontSize;
            charText.fontStyle = text.fontStyle;
            charText.lineSpacing = text.lineSpacing;
            charText.alignment = text.alignment;
            charText.color = text.color;
            charText.material = text.material;
            charText.text = c.ToString();
        }

        text.enabled = false;
        returnMotion = false;
    }      
   
    void Start () {
        canvas = GetComponentInParent<Canvas>();
        rt = GetComponent<RectTransform>();
        Transform[] chars = GetComponentsInChildren<Transform>();

        initialPos = rt.position;
        targetPos = initialPos; //initial values

        StartCoroutine(LateTargetPostitionCalculator());
    }

    //values imputed from ContentSizeFitter do not update until after first fram
    IEnumerator LateTargetPostitionCalculator()
    {
        yield return new WaitForEndOfFrame();

        targetPos.y += rippleSize * text.preferredHeight; // * canvas.scaleFactor;
    }

    void Update()
    {
        if (Mathf.Abs(rt.position.y - targetPos.y) > 1)
            rt.position = Vector3.Lerp(rt.position, targetPos, rippleSpeed * Time.deltaTime);
        else
        {
            returnMotion = true;
            rt.position = Vector3.Lerp(rt.position, initialPos, rippleSpeed * Time.deltaTime);
        }
    }
}
