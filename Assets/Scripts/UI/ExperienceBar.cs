﻿#pragma warning disable 414

using UnityEngine;
using UnityEngine.UI;
using Maelstrom.RPG;

namespace Maelstrom.UI
{
    public class ExperienceBar : MonoBehaviour
    {

        public Stats player;
        public Image fillImage;

        [SerializeField]
        int experience;
        float expNeeded, previousExp;

        void Start()
        {
            if (!player)
            {
                Debug.LogError("Entity not found.  Did you forget to attach it?");
            }

        }

        void Update()
        {
            GetExperience(player.level);
            experience = player.experience;
        }

        void GetExperience(int level)
        {
            expNeeded = GameLogic.ExpToNextLevel(level);
            previousExp = GameLogic.ExpToNextLevel(level - 1);

            fillImage.fillAmount = (player.experience - previousExp) / (expNeeded - previousExp);
        }
    }
}