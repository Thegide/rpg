﻿using UnityEngine;

namespace Maelstrom.RPG.UI
{

    public class CharacterCamera : MonoBehaviour
    {
        public Transform target;
        private Camera cam;
        public Vector3 offset;

        void Start()
        {
            cam = GetComponent<Camera>();
        }

        void LateUpdate()
        {
            Vector3 pos = target.position;
            pos += offset;

            cam.transform.position = pos;
        }
    }
}