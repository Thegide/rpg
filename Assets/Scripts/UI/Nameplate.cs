﻿using UnityEngine;
using UnityEngine.UI;

namespace Maelstrom.RPG.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class Nameplate : MonoBehaviour
    {
        public Text nameText;
        public Text titleText;
        public Color textColor = Color.white;

        public NPC npc;
        public float fadeDistance = 20;      

        CanvasGroup cg;

        void Start()
        {
            nameText.text = npc.Name;
            titleText.text = npc.Title;
            nameText.color = titleText.color = textColor;

            cg = GetComponent<CanvasGroup>();
        }

        void Update()
        {

            if (cg)
            {
                transform.LookAt(Camera.main.transform);
                transform.Rotate(0f, 180f, 0f);

                float distanceToCamera = Vector3.Distance(Camera.main.transform.position, transform.position);

                if (distanceToCamera > fadeDistance)
                {
                    cg.alpha = Mathf.Max(0f, 1 - (distanceToCamera - fadeDistance) / 10f);
                }
                else
                {
                    cg.alpha = 1;
                }
            }
        }
    }
}