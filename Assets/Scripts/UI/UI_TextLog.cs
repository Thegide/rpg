﻿using UnityEngine;
using UnityEngine.UI;

namespace Maelstrom.RPG.UI
{
    public class UI_TextLog : MonoBehaviour
    {
        public Transform contentWindow;
        public GameObject textPrefab;

        public static UI_TextLog instance;

        void Start()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
                Destroy(gameObject);
        }

        public void Log(string s)
        {
            if (s != "")
            {
                GameObject textObject = Instantiate(textPrefab, contentWindow);
                Text text = textObject.GetComponent<Text>();
                text.text = s;
            }
        }

        public void Log(string s, Color color)
        {
            if (s != "")
            {
                GameObject textObject = Instantiate(textPrefab, contentWindow);
                Text text = textObject.GetComponent<Text>();
                text.text = s;
                text.color = color;
            }
        }
    }
}