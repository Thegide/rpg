﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Maelstrom.RPG;

namespace Maelstrom.UI
{
    public class HPBarManager : MonoBehaviour
    {

        public GameObject BarPrefab;

        //List<Player> party;
        Player[] party;

        void Start()
        {
            //for debug only.... use runtime set which is managed.
            party = FindObjectsOfType<Player>();
            //party = partyArray.OfType<Player>().ToList();

            foreach (var player in party)
            {
                GameObject hpBar = Instantiate(BarPrefab, gameObject.transform);
                HealthBar barScript = hpBar.GetComponent<HealthBar>();
                barScript.entity = player;
            }
        }

        void Update()
        {

        }

        private void Add()
        {

        }

        private void Remove()
        {

        }
    }
}