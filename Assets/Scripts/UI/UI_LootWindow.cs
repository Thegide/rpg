﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Maelstrom.UI;

namespace Maelstrom.RPG.UI
{
    public class UI_LootWindow : UI_DynamicMenu
    {
        [Header ("Components")]
        public Text xpGained;
        public Text goldGained;
        public GameObject closeButton;      

        public void SetLootParams(int exp, int gold, List<Item> drops)
        {
            string _exp = String.Format("{0:n0}", exp);
            string _gold = String.Format("{0:n0}", gold);

            xpGained.text = "x" + _exp;
            goldGained.text = "x" + _gold;

            foreach (Item i in drops)
            {
                GameObject drop = Instantiate(buttonPrefab);
                drop.transform.SetParent(container.transform, false);

                UI_InventoryButton itemRow = drop.GetComponent<UI_InventoryButton>();
                itemRow.Icon.sprite = i.Icon;
                itemRow.Text.text = i.Name;
                itemRow.Quantity. text = 1.ToString();
            }
        }

        public new void Close()
        {
            base.Close();
        }
    }
}