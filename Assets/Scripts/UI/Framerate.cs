﻿using UnityEngine;
using UnityEngine.UI;

public class Framerate : MonoBehaviour
{
    public Text fpsText;

    void Start()
    {
        Text fpsText = GetComponent<Text>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        fpsText.text = (int)(1 / Time.deltaTime) + " FPS";
    }
}