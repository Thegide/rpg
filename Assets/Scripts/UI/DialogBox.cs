﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogBox : MonoBehaviour {

    //public GameObject textBox;
       
    public Text titleText;
    public Text dialogText;
    public TextAlignment alignment;
    public bool animate;

    public TextAsset textFile;
    string[] dialogArray;

    public int currentLine;
    public int lastLine;

    void Start ()
    {
	    if (textFile != null)
        {
            dialogArray = (textFile.text.Split('\n'));
        }

        if (lastLine == 0)
        {
            lastLine = dialogArray.Length - 1;
        }

        dialogText.text = "";
        StartCoroutine(AnimateText());
	}

    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            StopAllCoroutines();
            if (dialogText.text.Length < dialogArray[currentLine].Length)
            {
                dialogText.text = dialogArray[currentLine];
            }
            else
            {
                currentLine++;
                if (currentLine > lastLine)
                {                
                    gameObject.SetActive(false);
                    return;
                }

                StartCoroutine(AnimateText());
            }
        }
	}

    void LoadScript(TextAsset textFile)
    {
        if (textFile != null)
        {
            dialogArray = new string[1];
            dialogArray = textFile.text.Split('\n');
        }
    }

    IEnumerator AnimateText()
    {
        if (animate)
        {
            for (int i = 0; i <= dialogArray[currentLine].Length; i++)
            {
                dialogText.text = dialogArray[currentLine].Substring(0, i);
                yield return new WaitForSeconds(0.03f);
            }
        }
        else
        {
            dialogText.text = dialogArray[currentLine];
        }
    }
}
