﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Maelstrom.EventSystems;

namespace Maelstrom.UI
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CanvasGroup))]
    public class UI_Window : MonoBehaviour
    {
        #region Variables
        
        public enum windowAnimationStyle { fade, slide, expand }

        [Header("Animation Control")]
        public windowAnimationStyle WindowAnimationStyle = windowAnimationStyle.fade;
        [ReadOnly] public bool IsOpen = false;
        [ReadOnly] public bool HasFocus = false;

        [Header("Screen Events")]
        public UnityEvent onWindowOpen = new UnityEvent();
        public UnityEvent onWindowClose = new UnityEvent();
        public GameObject currentSelected;

        [HideInInspector]
        public bool inputCancel;
        [HideInInspector]
        public bool inputSelect;
        [HideInInspector]
        public bool inputTriggerLeft;
        [HideInInspector]
        public bool inputTriggerRight;

        private Animator animator;
        private CanvasGroup canvas;
        private UnityAction windowLostFocusListener;                
        #endregion
        
        void Awake()
        {
            windowLostFocusListener = new UnityAction(OnLostFocus);
            animator = GetComponent<Animator>();
            canvas = GetComponent<CanvasGroup>();
            currentSelected = GetComponentInChildren<Selectable>().gameObject;
        }

        // Update is called once per frame
        public void Update()
        {
            Inputs();

            if (HasFocus)
            {
                if (inputCancel)
                {
                    Close();
                }
                currentSelected = EventSystem.current.currentSelectedGameObject;
            }
        }

        protected void Inputs()
        {
            inputSelect = Input.GetButtonDown("A");
            inputCancel = Input.GetButtonDown("B");
            inputTriggerLeft = Input.GetButtonDown("L1");
            inputTriggerRight = Input.GetButtonDown("R1");
        }

        #region Animations

        public void Open()
        {
            gameObject.SetActive(true);
            if (IsOpen)
                return;

            IsOpen = true;
            HandleAnimator("show");
            Focus();

            if (onWindowOpen != null)
            {
                onWindowOpen.Invoke();
            }
        }

        public void Close()
        {
            if (!IsOpen)
                return;

            IsOpen = false;
            OnLostFocus();
            HandleAnimator("hide");

            if (onWindowClose != null)
            {
                onWindowClose.Invoke();
            }
        }

        void HandleAnimator(string trigger)
        {
            animator.SetTrigger(trigger);
            animator.SetInteger("state", (int)WindowAnimationStyle);
        }

        public void OnOpen()
        {
            canvas.interactable = true;
        }

        public void OnStartClose()
        {
            canvas.interactable = false;
        }

        public void OnClose()
        {
            gameObject.SetActive(false);
        }
        #endregion

        #region Event Handling

        public void Focus()
        {
            if (!HasFocus)
            {
                EventManager.TriggerEvent("WindowLostFocus");
                HasFocus = true;
                //Debug.Log(gameObject + " got focus");
                canvas.interactable = true;

                if (currentSelected == null)
                {
                    currentSelected = GetComponentInChildren<Selectable>().gameObject;
                }
                else
                {
                    Selectable selectable = currentSelected.GetComponent<Selectable>();
                    if (selectable != null)
                        selectable.Select();
                }
            }            
        }

        private void OnLostFocus()
        {
            if (HasFocus)
            {
                //Debug.Log(gameObject + " lost focus");
                HasFocus = false;
            }
            canvas.interactable = false;
        }

        public void OnEnable()
        {
            EventManager.StartListening("WindowLostFocus", windowLostFocusListener);
        }

        public void OnDisable()
        {
            EventManager.StopListening("WindowLostFocus", windowLostFocusListener);
        }

        #endregion
    }
}