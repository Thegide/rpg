﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Maelstrom.UI
{
    [RequireComponent(typeof(ScrollRect))]
    public class UI_Slider : MonoBehaviour
    {

        #region Variables
        public enum Direction { Horizontal, Vertical };

        public int startPage = 0;
        public float lerpSpeed = 10f;
        public GameObject prevButton, nextButton;
        public Direction direction = Direction.Horizontal;

        private ScrollRect _scrollRectComponent;
        private RectTransform _scrollRectRect;

        private RectTransform _container;
        public RectTransform Container { get { return _container; } }

        // number of pages in container
        private int _pageCount;
        private int _currentPage;

        // whether lerping is in progress and target lerp position
        private bool _lerp;
        private Vector2 _lerpTo;

        // target position of every page
        private List<Vector2> _pagePositions = new List<Vector2>();
        #endregion

        #region Main Methods
        void Awake()
        {
            _scrollRectComponent = GetComponent<ScrollRect>();
            _scrollRectRect = GetComponent<RectTransform>();
            _container = _scrollRectComponent.content;

            _lerp = false;

            Initialize();

            // Event listeners for prev and next buttons
            if (nextButton)
                nextButton.GetComponent<Button>().onClick.AddListener(() => { NextScreen(); });

            if (prevButton)
                prevButton.GetComponent<Button>().onClick.AddListener(() => { PreviousScreen(); });
        }

        // Update is called once per frame
        void Update()
        {
            // if moving to target position
            if (_lerp)
            {
                // prevent overshooting with values greater than 1
                float decelerate = Mathf.Min(lerpSpeed * Time.deltaTime, 1f);
                _container.anchoredPosition = Vector2.Lerp(_container.anchoredPosition, _lerpTo, decelerate);
                // time to stop lerping?
                if (Vector2.SqrMagnitude(_container.anchoredPosition - _lerpTo) < 0.25f)
                {
                    // snap to target and stop lerping
                    _container.anchoredPosition = _lerpTo;
                    _lerp = false;
                    // clear also any scrollrect move that may interfere with our lerping
                    _scrollRectComponent.velocity = Vector2.zero;
                }
            }
        }

        public void Initialize()
        {
            _pageCount = _container.childCount;
            if (_pageCount > 0)
            {
                SetPagePositions();
                SetPage(startPage);
            }
        }
        #endregion

        #region Helper Methods
        protected void SetPagePositions()
        {
            int width = 0;
            int height = 0;
            int offsetX = 0;
            int offsetY = 0;
            int containerWidth = 0;
            int containerHeight = 0;

            if (direction == Direction.Horizontal)
            {
                // screen width in pixels of scrollrect window
                width = (int)_scrollRectRect.rect.width;
                // center position of all pages
                offsetX = width / 2;              
                // total width
                containerWidth = width * _pageCount;
            }
            else
            {
                height = (int)_scrollRectRect.rect.height;
                offsetY = height / 2;
                containerHeight = height * _pageCount;
            }

            // set width of container
            Vector2 newSize = new Vector2(containerWidth, containerHeight);
            _container.sizeDelta = newSize;
            Vector2 newPosition = new Vector2(containerWidth / 2, containerHeight / 2);
            _container.anchoredPosition = newPosition;

            // delete any previous settings
            _pagePositions.Clear();

            // iterate through all container childern and set their positions
            for (int i = 0; i < _pageCount; i++)
            {
                RectTransform child = _container.GetChild(i).GetComponent<RectTransform>();
                Vector2 childPosition;
                if (direction == Direction.Horizontal)
                {
                    childPosition = new Vector2(i * width - containerWidth / 2 + offsetX, 0f);
                }
                else
                {
                    childPosition = new Vector2(0f, -(i * height - containerHeight / 2 + offsetY));
                }
                child.anchoredPosition = childPosition;
                _pagePositions.Add(-childPosition);
            }
        }

        protected void SetPage(int aPageIndex)
        {
            aPageIndex = Mathf.Clamp(aPageIndex, 0, _pageCount - 1);
            _container.anchoredPosition = _pagePositions[aPageIndex];
            _currentPage = aPageIndex;
        }

        private void LerpToPage(int aPageIndex)
        {
            aPageIndex = Mathf.Clamp(aPageIndex, 0, _pageCount - 1);
            _lerpTo = _pagePositions[aPageIndex];
            _lerp = true;
            _currentPage = aPageIndex;
        }

        private void NextScreen()
        {
            LerpToPage(_currentPage + 1);
        }

        private void PreviousScreen()
        {
            LerpToPage(_currentPage - 1);
        }

        private int GetNearestPage()
        {
            // based on distance from current position, find nearest page
            Vector2 currentPosition = _container.anchoredPosition;

            float distance = float.MaxValue;
            int nearestPage = _currentPage;

            for (int i = 0; i < _pagePositions.Count; i++)
            {
                float testDist = Vector2.SqrMagnitude(currentPosition - _pagePositions[i]);
                if (testDist < distance)
                {
                    distance = testDist;
                    nearestPage = i;
                }
            }

            return nearestPage;
        }

        #endregion
    }
}