﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using Maelstrom.EventSystems;

namespace Maelstrom.UI
{

    [RequireComponent(typeof(Animator))]  //for transitions
    [RequireComponent(typeof(CanvasGroup))]  //allows to control transparency 
    public class UI_DynamicMenu : MonoBehaviour
    {
        #region Variables

        [Header("Main Properties")]
        public Selectable m_StartSelectable;

        [HideInInspector]
        public bool IsOpen = false;
        public GameObject container;
        public GameObject buttonPrefab;

        private UI_DynamicMenu parentMenu;
        public UI_DynamicMenu ParentMenu { get { return parentMenu; } }

        public bool rememberSelection = false;
        private int lastButtonSelectedId;
        
        [Header("Screen Events")]
        public UnityEvent onMenuOpen = new UnityEvent();
        public UnityEvent onMenuClose = new UnityEvent();
        private Animator animator;

        [Header("Fader")]
        public float fadeTime;
        public float displayTime;
        public Text displayText;
        private IEnumerator fadeAlpha;

        //event system
        private UnityAction closeMenuListener;
        #endregion

        #region Main Methods
        public virtual void Awake()
        {
            animator = GetComponent<Animator>();

            if (m_StartSelectable)
            {
                EventSystem.current.SetSelectedGameObject(m_StartSelectable.gameObject);
            }

            //transform.SetParent(FindObjectOfType<Canvas>().transform);

            lastButtonSelectedId = 0; //for debugging purposes - memory cursor

            closeMenuListener = new UnityAction(Close);
        }

        public virtual void Update() { }

        public void Open()
        {
            if (onMenuOpen != null)
            {
                onMenuOpen.Invoke();
            }
          
            IsOpen = true;
            HandleAnimator("show");
            EventManager.StartListening("CloseMenu", closeMenuListener);
        }

        public void Close()
        {
            if (onMenuClose != null)
            {
                onMenuClose.Invoke();
            }           

            IsOpen = false;
            HandleAnimator("hide");
            Clear();
            EventManager.StopListening("CloseMenu", closeMenuListener);            
        }

        #endregion

        #region Helper Methods

        public void Populate(string[] labels)
        {
            Clear();

            Button[] buttons = new Button[labels.Length];
            for (int i = 0; i < labels.Length; i++)
            {
                GameObject buttonObject = Instantiate(buttonPrefab) as GameObject;
                buttonObject.transform.SetParent(container.transform, false);
                buttonObject.GetComponentInChildren<Text>().text = labels[i];
                Button button = buttonObject.GetComponent<Button>();
                buttons[i] = button;

                var id = i;

                button.onClick.AddListener(delegate { DoButtonAction(labels[id]); });
            }

            if (buttons.Length > 0)
            {
                if (rememberSelection)
                {
                    buttons[lastButtonSelectedId].Select();
                }
                else
                {
                    buttons[0].Select();
                }
            }
        }

        public virtual void DoButtonAction(string name)
        {
            Debug.Log(name + " clicked");
        }

        void HandleAnimator(string trigger)
        {
            animator.SetTrigger(trigger);
        }

        public void Clear()
        {
            var buttons = GetComponentsInChildren<Button>();
            foreach (Button b in buttons)
            {
                b.onClick.RemoveAllListeners();
                Destroy(b.gameObject);  
            }
        }

        public void Enable()
        {
            var buttons = GetComponentsInChildren<Button>();
            foreach (Button b in buttons)
            {
                b.interactable = true;
            }

            if (buttons.Length > 0)
            {
                buttons[0].Select();
            }
        }

        public void Disable()
        {
            var buttons = GetComponentsInChildren<Button>();
            foreach (Button b in buttons)
            {
                b.interactable = false;
            }
        }

        public void SetParentMenu(UI_DynamicMenu menu)
        {
            if (menu)
            {
                parentMenu = menu;
            }
        }

        #endregion

        #region Fader    

        void SetAlpha()
        {
            if (fadeAlpha != null)
            {
                StopCoroutine(fadeAlpha);
            }
            fadeAlpha = FadeAlpha();
            StartCoroutine(fadeAlpha);
        }

        IEnumerator FadeAlpha()
        {
            Color resetColor = displayText.color;
            resetColor.a = 1;
            displayText.color = resetColor;

            yield return new WaitForSeconds(displayTime);

            while (displayText.color.a > 0)
            {
                Color displayColor = displayText.color;
                displayColor.a -= Time.deltaTime / fadeTime;
                displayText.color = displayColor;
                yield return null;
            }
            yield return null;
        }
        #endregion

    }
}