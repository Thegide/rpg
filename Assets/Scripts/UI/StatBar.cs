﻿using UnityEngine;
using UnityEngine.UI;

namespace Maelstrom.UI
{
    public class StatBar : MonoBehaviour {

        public Image fillImage;

        private float percentage = 0f;
        public float Percentage { set { percentage = value;} }
        
        void Update() {
            fillImage.fillAmount = Mathf.Clamp01(percentage);
        }
    }
}
