﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Maelstrom.UI
{
    //This extension is for buttons with an invisible background.  It highlights the text using the 
    //ColorBlock specified colors for the parent button.
    public class TransparentButtonGlowExtension : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, ISelectHandler, IDeselectHandler
    {

        protected ColorBlock colors;
        protected Button button;
        protected Text buttonText;

        void Awake()
        {
            button = gameObject.GetComponent<Button>();
            buttonText = button.GetComponentInChildren<Text>();
            colors = button.colors;
            buttonText.color = colors.normalColor;
        }

        public void OnSelect(BaseEventData eventData)
        {
            buttonText.color = colors.highlightedColor;
        }

        public void OnDeselect(BaseEventData eventData)
        {
            buttonText.color = colors.normalColor;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            buttonText.color = colors.highlightedColor;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            buttonText.color = colors.normalColor;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            buttonText.color = colors.pressedColor;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            buttonText.color = colors.normalColor;
        }
    }

}