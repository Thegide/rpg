﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Maelstrom.UI
{
    public class ButtonScaler : MonoBehaviour, ISelectHandler, IDeselectHandler
    {
        public float scale = 1f;

        Vector2 offMax;
        Vector2 offMin;
        RectTransform rect;

        void Awake()
        {
            rect = transform as RectTransform;
            offMax = rect.offsetMax;
            offMin = rect.offsetMin;
        }

        public void OnSelect(BaseEventData data)
        {
            rect.offsetMax = offMax * scale;
            rect.offsetMin = offMin * scale;
        }

        public void OnDeselect(BaseEventData data)
        {
            rect.offsetMax = offMax;
            rect.offsetMin = offMin;
        }
    }
}