﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace Maelstrom.UI
{
    //This extension is for buttons with an invisible background.  It highlights the text using the 
    //ColorBlock specified colors for the parent button.
    public class TransparentButtonExtension : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, ISelectHandler, IDeselectHandler
    {
        public enum EffectType { TextColor, OuterGlow }
        public EffectType effectType = EffectType.TextColor;

        protected ColorBlock colors;
        protected Button button;
        protected Text buttonText;
        protected Outline outline;
        protected Color defaultOutlineColor;

        bool isHighlighted = false;

        void Awake()
        {
            button = gameObject.GetComponent<Button>();
            buttonText = button.GetComponentInChildren<Text>();
            outline = button.GetComponentInChildren<Outline>();
            colors = button.colors;
            buttonText.color = colors.normalColor;

            if (outline == null)
            {
                outline = buttonText.gameObject.AddComponent<Outline>();
            }
            defaultOutlineColor = outline.effectColor;
        }

        public void OnSelect(BaseEventData eventData)
        {
            isHighlighted = true;
            if (effectType == EffectType.TextColor)
            {
                buttonText.color = colors.highlightedColor;
            }
            else if (effectType == EffectType.OuterGlow)
            {
                outline.effectColor = colors.highlightedColor;
            }
        }

        public void OnDeselect(BaseEventData eventData)
        {
            isHighlighted = false;
            if (effectType == EffectType.TextColor)
            {
                buttonText.color = colors.normalColor;
            }
            else if (effectType == EffectType.OuterGlow)
            {
                outline.effectColor = defaultOutlineColor;
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            isHighlighted = true;
            if (effectType == EffectType.TextColor)
            {
                buttonText.color = colors.highlightedColor;
            }
            else if (effectType == EffectType.OuterGlow)
            {
                outline.effectColor = colors.highlightedColor;
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            isHighlighted = false;
            if (effectType == EffectType.TextColor)
            {
                buttonText.color = colors.normalColor;
            }
            else if (effectType == EffectType.OuterGlow)
            {
                outline.effectColor = defaultOutlineColor;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (effectType == EffectType.TextColor)
            {
                buttonText.color = colors.pressedColor;
            }
            else if (effectType == EffectType.OuterGlow)
            {
                outline.effectColor = colors.pressedColor;
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {

            if (effectType == EffectType.TextColor)
            {
                if (isHighlighted)
                    buttonText.color = colors.highlightedColor;
                else
                    buttonText.color = colors.normalColor;
            }
            else if (effectType == EffectType.OuterGlow)
            {
                if (isHighlighted)
                    outline.effectColor = colors.highlightedColor;
                else
                    outline.effectColor = defaultOutlineColor;
            }

        }
    }

}