﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonToggleExtension : MonoBehaviour {

    public GameObject ObjectToToggle;
    private bool initState;

    public void OnClick()
    {
        if (ObjectToToggle)
        {
            initState = ObjectToToggle.activeSelf;
            ObjectToToggle.SetActive(!initState);
        }
    }
}
