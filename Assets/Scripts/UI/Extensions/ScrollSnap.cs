﻿#pragma warning disable CS0414 //ignore assigned but unused variables

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Maelstrom.UI
{
    //Scrollable menu that can be manipulated by a variety of control schemes.
    public class ScrollSnap : MonoBehaviour
    {
        #region Variables
        [SerializeField]
        private float _offsetX = 0f, _offsetY = 0f;
        
        private ScrollRect _scrollRectComponent;
        private RectTransform _scrollRectRect;
        
        private RectTransform _viewport;
        private RectTransform _content;
        [SerializeField]
        private int _pageCount;

        #endregion

        void Awake()
        {
            _scrollRectComponent = GetComponent<ScrollRect>();
            _scrollRectRect = GetComponent<RectTransform>();
            _viewport = _scrollRectComponent.viewport;
            _content = _scrollRectComponent.content;

            GridLayoutGroup grid = GetComponentInChildren<GridLayoutGroup>();
            if (grid)
            {
                _offsetX = grid.cellSize.x + grid.spacing.x;
                _offsetY = grid.cellSize.y + grid.spacing.y;
            }
        }

        void Start()
        {
            _pageCount = _content.childCount;
        }


        // Update is called once per frame
        void Update()
        {            
            GameObject selectedObject = EventSystem.current.currentSelectedGameObject;
            if (selectedObject)
            {
                if (selectedObject.transform.IsChildOf(_content))
                {
                    ScrollToButton(selectedObject.GetComponent<RectTransform>());                    
                }
            }
        }

        #region Helper Methods

        RectTransform GetSelectedButtonRect()
        {
            GameObject selected = EventSystem.current.currentSelectedGameObject;
            if (selected)
            {                
                return selected.GetComponent<RectTransform>();
            }

            else return null;
        }

        void ScrollToButton(RectTransform buttonRect)
        {
            Vector2 viewportMinCorner;
            Vector2 viewportMaxCorner;

            if (_viewport != null)
            {
                //first get corners of our viewport rect in worldspace
                Vector3[] v_wcorners = new Vector3[4];
                _viewport.GetWorldCorners(v_wcorners); //bot left, top left, top right, bot right
               
                viewportMinCorner = v_wcorners[0];
                viewportMaxCorner = v_wcorners[2];
            }
            else
            {
                //just use the scren as the viewport
                viewportMinCorner = new Vector2(0, 0);
                viewportMaxCorner = new Vector2(Screen.width, Screen.height);
            }

            //give 1 pixel border to avoid numeric issues:
            viewportMinCorner += Vector2.one;
            viewportMaxCorner -= Vector2.one;

            //get button corners in worldspace
            Vector3[] e_wcorners = new Vector3[4];
            buttonRect.GetWorldCorners(e_wcorners);

            Vector2 elem_minCorner = e_wcorners[0];
            Vector2 elem_maxCorner = e_wcorners[2];

            //test to see if button corners fall outside of viewport rect
            if (elem_maxCorner.x > viewportMaxCorner.x)
            {
                _content.position = _content.position - new Vector3(_offsetX, 0f);
            }

            if (elem_maxCorner.y > viewportMaxCorner.y)
            {     
                _content.position = _content.position - new Vector3(0f, _offsetY);
            }

            if (elem_minCorner.x < viewportMinCorner.x)
            {
                _content.position = _content.position + new Vector3(_offsetX, 0f);
            }

            if (elem_minCorner.y < viewportMinCorner.y)
            {
                _content.position = _content.position + new Vector3 (0f, _offsetY, 0f);
            }
            
        }

        #endregion
    }
}