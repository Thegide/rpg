﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameSceneManager : MonoBehaviour
{

    public static GameSceneManager instance;
    public Animator anim;

    private string sceneToLoad;

    void Start()
    {
        if (instance != null)
        {
            GameObject.Destroy(gameObject);
        }
        else
        {
            GameObject.DontDestroyOnLoad(gameObject);
            instance = this;
        }

        anim = GetComponentInChildren<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        //for debug only

        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            LoadWorldScene();
        }
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            LoadBattleScene();
        }
    }

    public void FadeToScene(string sceneName)
    {
        sceneToLoad = sceneName;
        anim.SetTrigger("FadeOut");
    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(sceneToLoad);
        anim.SetTrigger("FadeIn");
    }


    public void LoadWorldScene()
    {

        FadeToScene("WorldScene");
    }

    public void LoadBattleScene()
    {
        FadeToScene("BattleScene");
    }


}

