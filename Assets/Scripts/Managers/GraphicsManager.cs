﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphicsManager : MonoBehaviour {

    public Dropdown qualityDropdown;
    public Dropdown resDropdown;
    public Dropdown fullscreenDropdown;
    public Slider brightnessSlider;
    public Dropdown vSyncDropdown;
    
    int m_screenHeight;
    int m_screenWidth;
    bool m_fullScreen;
    float m_brightness = 1.0f;
    int m_vSync;
    Resolution[] resArray;

    public Shader brightShader;
    Material m_Material;

    private void Awake()
    {
        InitResolutions();

        m_screenWidth = Screen.width;
        m_screenHeight = Screen.height;        
        m_fullScreen = Screen.fullScreen;
        m_vSync = QualitySettings.vSyncCount;

        //set options in menu to current settings
        //qualityDropdown = 1;
        resDropdown.value = resDropdown.options.FindIndex(text => text.Equals(m_screenWidth + "x" + m_screenHeight));
        //TODO:  Screen.fullScreenMode for windowed, fullscreen, maximized, borderless
        fullscreenDropdown.value = Convert.ToInt16(m_fullScreen);
        vSyncDropdown.value = m_vSync;
        brightnessSlider.value = 0.5f * m_brightness;

        DisplaySliderValue setBrightText = brightnessSlider.gameObject.transform.parent.GetComponentInChildren<DisplaySliderValue>();
        setBrightText.SetValue(brightnessSlider);
    }


    public void Brightness(Slider slider)
    {
        m_brightness = slider.value * 2.0f;
    }

    public void Resolution(Dropdown res)
    {
        int index = res.value;
        Screen.SetResolution(resArray[index].width, resArray[index].height, m_fullScreen);
        Debug.Log("Resolution set to " + res.options[res.value].text);
    }

    public void FullScreen(Dropdown dropdown)
    {
        bool fs = Convert.ToBoolean(dropdown.value);

        m_fullScreen = fs;
        Screen.SetResolution(m_screenWidth, m_screenHeight, m_fullScreen);
        Debug.Log("Fullscreen mode is " + (m_fullScreen ? "enabled" : "disabled"));
    }

    /*When building a player quality levels that are not used for that platform are stripped. 
     * You should not expect a given quality setting to be at a given index. It's best to query 
     * the available quality settings and use the returned index.
     */
    public void Quality(Dropdown qual)
    {
        int qualityLevel = qual.value;
        QualitySettings.SetQualityLevel(qualityLevel);
        Debug.Log("Quality set to " + QualitySettings.GetQualityLevel());
    }

    public void VSync(Dropdown dropdown)
    {
        m_vSync = dropdown.value;
        QualitySettings.vSyncCount = m_vSync;
        Debug.Log("VSync set to " + QualitySettings.vSyncCount);
    }

    //Populates the dropdown list with available resolutions
    public void InitResolutions()
    {
        resArray = Screen.resolutions;
        Array.Reverse(resArray);
        List<string> resolutions = new List<string>();
        resDropdown.ClearOptions();
        foreach (Resolution r in resArray)
        {
            resolutions.Add(r.width + " x " + r.height);
        }
        resDropdown.AddOptions(resolutions);
    }

    #region Post-Processing Effects
    [ImageEffectOpaque]
    void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        if (m_Material == null)
        {
            m_Material = new Material(brightShader);
        }

        m_Material.SetFloat("_Brightness", m_brightness);
        Graphics.Blit(src, dst, m_Material);
    }

    void OnDisable()
    {
        if (m_Material)
        {
            DestroyImmediate(m_Material);
        }
    }
    #endregion
}
