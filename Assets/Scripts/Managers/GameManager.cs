﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Maelstrom.RPG.UI;
using Maelstrom.UI;


namespace Maelstrom.RPG
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance = null;
        public static bool GameIsPaused = false;

        public UI_MainMenuManager mainMenu;
        public GameObject mainMenuUI;
        public GameObject arrowPrefab;

        #region Variables
        public List<Player> party;
        public Inventory playerInventory;


        private List<Character> npcs;
        public List<Character> NPCs { get { return npcs; } }
        public Character currentTarget;
        private GameObject targetArrow;

        #endregion

        #region Main Methods

        void Awake()
        {
            if (instance == null)
            {
                //DontDestroyOnLoad(gameObject);
                instance = this;
            }
            else if (instance != this)
                Destroy(gameObject);

            party = gameObject.GetComponentsInChildren<Player>(true).ToList();

            mainMenuUI.GetComponent<UI_Window>().Close();
        }

        // Update is called once per frame
        void Update()
        {
            if ((Input.GetButtonDown("Start") || Input.GetKeyDown(KeyCode.Escape)) && !GameIsPaused)
            {                
                Pause();
                mainMenuUI.GetComponent<UI_Window>().Open();
            }
            
            //UpdateTargets();
        }

        public void Pause()
        {            
            GameIsPaused = true;
        }

        //control returned to the game by main menu close screen event
        public void Resume()
        {
            GameIsPaused = false;
        }

        #endregion

        //#region Targeting

        //public Character GetPreviousTarget()
        //{

        //    SortTargets();
        //    int index = npcs.IndexOf(currentTarget);

        //    if (index < 0)  //not found
        //    {
        //        currentTarget = npcs.First();
        //    }
        //    else
        //    {
        //        index--;
        //        if (index < 0)
        //        {
        //            index = npcs.Count - 1;
        //        }

        //        currentTarget = npcs[index];
        //    }

        //    if (targetArrow == null)
        //    {
        //        targetArrow = Instantiate(arrowPrefab);
        //        targetArrow.transform.SetParent(currentTarget.transform);
        //        targetArrow.transform.position += Vector3.up;
        //    }
        //    else
        //    {
        //        targetArrow.transform.SetParent(currentTarget.transform);
        //        targetArrow.transform.position = currentTarget.transform.position + Vector3.up;
        //    }

        //    return currentTarget;
        //}

        //public Character GetNextTarget()
        //{
        //    SortTargets();
        //    int index = npcs.IndexOf(currentTarget);

        //    if (currentTarget == null)
        //    {
        //        currentTarget = npcs.First();
        //    }
        //    else
        //    {
        //        index++;
        //        index %= npcs.Count;

        //        currentTarget = npcs[index];
        //    }

        //    if (targetArrow == null)
        //    {
        //        targetArrow = Instantiate(arrowPrefab);
        //        targetArrow.transform.SetParent(currentTarget.transform);
        //        targetArrow.transform.position += Vector3.up;
        //    }
        //    else
        //    {
        //        targetArrow.transform.SetParent(currentTarget.transform);
        //        targetArrow.transform.position = currentTarget.transform.position + Vector3.up;
        //    }

        //    return currentTarget;
        //}

        //public void ClearTarget()
        //{
        //    currentTarget = null;

        //    if (targetArrow)
        //        Destroy(targetArrow);
        //}

        //void UpdateTargets()
        //{
        //    npcs = FindObjectsOfType<Character>().ToList();
        //}

        //void SortTargets()
        //{
        //    npcs.Sort(delegate (Character a, Character b)
        //    {
        //        return Vector2.Distance(this.transform.position, a.transform.position)
        //        .CompareTo(
        //          Vector2.Distance(this.transform.position, b.transform.position));
        //    });
        //}
          
        //#endregion

        #region Inputs



        #endregion

    }

}
