﻿using UnityEngine;

namespace Maelstrom.RPG
{
    //the GameLogic class defines the growth curves for experience and stats
    public class GameLogic
    {
        public static int ExpToNextLevel(int level)
        {
            //experience calculation is polynomial 
            int exp = (level * level * level) * 8 + 92; // 8x^3 + 92 

            return exp;
        }

        public static int CalcLinearStat(int level, float growthRate)
        {
            float stat = growthRate * level + 4;

            return Mathf.RoundToInt(stat);
        }

        public static int CalcPolynomialStat(int level, float growthRate)
        {
            float stat = Mathf.Pow((float)level, growthRate) + 4;

            return Mathf.RoundToInt(stat);
        }


    }
}
