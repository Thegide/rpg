﻿using UnityEngine;
using UnityEngine.AI;

namespace Maelstrom.RPG
{    
    public class Interactable : MonoBehaviour
    {
        [ReadOnly] public GameObject interactingObject;

        public virtual void Awake()
        {
            gameObject.tag = "Interactable";

        }

        //optimization todo:  how to handle player colliding with multiple interactables at once?
        public virtual void OnTriggerEnter(Collider other)
        {
            //optimization todo: reassign controller & collider to base player object
            //if (other.gameObject.CompareTag("Player"))

            if (other.gameObject.GetComponentInParent<Player>() != null)
            {
                interactingObject = other.gameObject;
            }
        }

        public virtual void OnTriggerExit(Collider other)
        {
            interactingObject = null;
            Exit();
        }

        public virtual void Interact()
        {
            Debug.Log("Interacted with " + interactingObject.name);
        }

        public virtual void Exit()
        {            
           
        }
    }
}