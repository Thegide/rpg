﻿using UnityEngine;
using Maelstrom.RPG.UI;

namespace Maelstrom.RPG {

    [RequireComponent(typeof(Animator))]
    public class TreasureChest : Interactable
    {
        Animator anim;
        [SerializeField]
        bool empty = false;
        bool open = false;
        public GameObject contents;

        public LootDropTable loot;
        
        void Start()
        {
            anim = GetComponent<Animator>();
            contents.SetActive(true);
        }

        void Update()
        {
            if (open && interactingObject == null)
                Close();
        }

        public override void Interact()
        {
            if (!open)
                Open();   
            else
                Close();
        }

        //Animation Trigger
        void GiveItems()
        {
            if (empty) { return; }

            foreach (LootDrop drop in loot.lootDropItems)
            {
                //Debug.Log("Found: " + drop.item.Name);
                if (GameManager.instance)
                {
                    GameManager.instance.playerInventory.Add(drop.item);                    
                    UI_TextLog.instance.Log("Picked up " + drop.item.Name);
                }
            }

            empty = true;
            contents.SetActive(false);
        }

        #region Animation

        void Open()
        {
            anim.SetTrigger("open");
            open = true;
        }

        void Close()
        {
            anim.SetTrigger("close");
            open = false;
        }
        #endregion
    }
}