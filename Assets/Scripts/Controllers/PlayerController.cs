﻿#pragma warning disable CS0414 //ignore unused input variables (cam, select, cancel)

using UnityEngine;
using System.Collections;
using Maelstrom.RPG.UI;

namespace Maelstrom.RPG
{

    public enum Pose
    {
        UNARMED = 0,
        TWOHANDSWORD = 1,
        TWOHANDSPEAR = 2,
        TWOHANDAXE = 3,
        TWOHANDBOW = 4,
        TWOHANDCROSSBOW = 5,
        STAFF = 6,
        ARMED = 7,
        RELAX = 8,
        RIFLE = 9,
        TWOHANDCLUB = 10,
        SHIELD = 11,
        ARMEDSHIELD = 12
    }

    //Script requires Rigidbody and NavMeshAgent components.
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    public class PlayerController : MonoBehaviour
    {

        #region Variables

        //Components.
        Rigidbody rb;
        protected Animator animator;
        public GameObject target;

        [HideInInspector]
        public Vector3 targetDashDirection;
        public Camera sceneCamera;
        public float camFollowDistance = 10f;
        public float maxCamHeight = 15f;
        private UnityEngine.AI.NavMeshAgent agent;

        //Movement.
        [HideInInspector]
        public bool canMove = true;
        public float walkSpeed = 1.35f;
        float moveSpeed;
        public float runSpeed = 6f;
        float rotationSpeed = 40f;
        Vector3 inputVec;
        Vector3 newVelocity;
        public float slopeAmount = 0.5f;
        public bool onAllowableSlope;

        //Navmesh.
        public bool useNavMesh = false;
        private float navMeshSpeed;
        public Transform goal;

        //Jumping.
        public float gravity = -9.8f;
        [HideInInspector]
        public bool canJump;
        bool isJumping = false;
        [HideInInspector]
        public bool isGrounded;
        public float jumpSpeed = 12;

        bool isFalling;
        bool startFall;
        float fallingVelocity = -1f;
        float fallTimer = 0f;
        public float fallDelay = 0.2f;
        float distanceToGround;

        //Movement in the air.
        public float inAirSpeed = 8f;
        float maxVelocity = 2f;
        float minVelocity = -2f;

        //Weapon and Shield
        [HideInInspector]
        public Pose pose;
        int rightWeapon = 0;
        int leftWeapon = 0;
        [HideInInspector]
        public bool isRelax = false;

        //Actions.
        [HideInInspector]
        public bool canAction = true;
        public bool menuIsOpen = false;
        public bool isPaused = false;

        //Input variables.
        float inputHorizontal = 0f;
        float inputVertical = 0f;

        //float inputCamHorizontal = 0f;
        //float inputCamVertical = 0f;
        bool inputSelect;
        bool inputCancel;
        bool inputJump;
        bool inputMenu;
        bool inputPause;
        bool inputTriggerL;
        bool inputTriggerR;

        //interaction
        [ReadOnly]public Interactable interactingObject = null;
        bool isInteracting = false;

        #endregion

        #region Initialization and Inputs

        void Start()
        {
            //Find the Animator component.
            if (animator = GetComponentInChildren<Animator>())
            {
            }
            else
            {
                Debug.LogError("ERROR: There is no animator for character.");
                Destroy(this);
            }
            //Use MainCamera if no camera is selected.
            if (sceneCamera == null)
            {
                sceneCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
                if (sceneCamera == null)
                {
                    Debug.LogError("ERROR: There is no camera in scene.");
                    Destroy(this);
                }
            }

            rb = GetComponent<Rigidbody>();
            agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
            agent.enabled = false;
        }

        private void OnTriggerEnter(Collider collider)
        {
            //eventually replace with code that allows for a list of triggering objects
            if (collider.gameObject.tag == "Interactable")
            {
                interactingObject = collider.gameObject.GetComponent<Interactable>();
                isInteracting = true;
            }
        }

        private void OnTriggerExit(Collider collider)
        {
            interactingObject = null;
            isInteracting = false;
        }


        /// <summary>
        /// Input abstraction for easier asset updates using outside control schemes.
        /// </summary>
        void Inputs()
        {
            //inputCamHorizontal = Input.GetAxisRaw("Horizontal R");  //managed by CameraRotator.cs
            //inputCamVertical = Input.GetAxisRaw("Vertical R");
            inputHorizontal = Input.GetAxisRaw("Horizontal");
            inputVertical = Input.GetAxisRaw("Vertical");

            inputPause = Input.GetButtonDown("Start");
            inputSelect = Input.GetButtonDown("A");
            inputCancel = Input.GetButtonDown("B");
            inputJump = Input.GetButtonDown("Y");
            inputMenu = Input.GetButtonDown("X");
            inputTriggerR = Input.GetButtonDown("R1");
            inputTriggerL = Input.GetButtonDown("L1");
        }

        #endregion

        #region Updates

        void Update()
        {
            if (GameManager.GameIsPaused)
            {
                return;
            }

            Inputs();
            if (isInteracting && inputSelect)
            {
                interactingObject.Interact();
            }

            if (canMove && !useNavMesh)
            {
                CameraRelativeMovement();
            }

            Jumping();

            //Navmesh.
            if (useNavMesh)
            {
                agent.enabled = true;
                navMeshSpeed = agent.velocity.magnitude;
            }
            else
            {
                agent.enabled = false;
            }

            //Navigate to click.
            if (Input.GetMouseButtonDown(0))
            {
                if (useNavMesh)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
                    {
                        agent.destination = hit.point;
                    }
                }
            }

            //Targeting();

        }

        void FixedUpdate()
        {
            CheckForGrounded();
            //Apply gravity.
            rb.AddForce(0, gravity, 0, ForceMode.Acceleration);
            AirControl();
            //Check if character can move.
            if (canMove)
            {
                moveSpeed = UpdateMovement();
            }
            //Check if falling.
            if (rb.velocity.y < fallingVelocity && useNavMesh != true)
            {
                isFalling = true;
                animator.SetInteger("Jumping", 2);
                canJump = false;
            }
            else
            {
                isFalling = false;
            }

        }

        /// <summary>
        /// Get velocity of rigid body and pass the value to the animator to control the animations.
        /// </summary>
        void LateUpdate()
        {
            if (!useNavMesh)
            {
                //Get local velocity of charcter
                float velocityXel = transform.InverseTransformDirection(rb.velocity).x;
                float velocityZel = transform.InverseTransformDirection(rb.velocity).z;
                //Update animator with movement values
                animator.SetFloat("Velocity X", velocityXel / runSpeed);
                animator.SetFloat("Velocity Z", velocityZel / runSpeed);
                //if character is alive and can move, set our animator
                if (canMove)
                {
                    if (moveSpeed > 0)
                    {
                        animator.SetBool("Moving", true);
                    }
                    else
                    {
                        animator.SetBool("Moving", false);
                    }
                }
            }
            else
            {
                animator.SetFloat("Velocity X", agent.velocity.sqrMagnitude);
                animator.SetFloat("Velocity Z", agent.velocity.sqrMagnitude);
                if (navMeshSpeed > 0)
                {
                    animator.SetBool("Moving", true);
                }
                else
                {
                    animator.SetBool("Moving", false);
                }
            }
        }

        #endregion

        #region UpdateMovement

        /// <summary>
        /// Movement based off camera facing.
        /// </summary>
        void CameraRelativeMovement()
        {
            //converts control input vectors into camera facing vectors.
            Transform cameraTransform = sceneCamera.transform;
            //Forward vector relative to the camera along the x-z plane.
            Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);
            forward.y = 0;
            forward = forward.normalized;
            //Right vector relative to the camera always orthogonal to the forward vector.
            Vector3 right = new Vector3(forward.z, 0, -forward.x);

            if (GameManager.GameIsPaused)
            {
                inputVec = Vector3.zero;
            }
            else
            {
                inputVec = inputHorizontal * right + inputVertical * forward;
            }
        }

        /// <summary>
        /// Rotate character towards movement direction.
        /// </summary>
        void RotateTowardsMovementDir()
        {
            if (inputVec != Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(inputVec), Time.deltaTime * rotationSpeed);
            }
        }

        /// <summary>
        /// Applies velocity to rigidbody to move the character, and controls rotation if not targetting.
        /// </summary>
        /// <returns>The movement.</returns>
        float UpdateMovement()
        {
            if (!useNavMesh && !menuIsOpen)
            {
                CameraRelativeMovement();
            }
            Vector3 motion = inputVec;
            if (isGrounded)
            {
                //Reduce input for diagonal movement.
                if (motion.magnitude > 1)
                {
                    motion.Normalize();
                }
                if (canMove && !menuIsOpen)
                {
                    //Set speed by walking / running.                    
                    newVelocity = motion * runSpeed;
                }
            }
            else
            {
                //If falling, use momentum.
                newVelocity = rb.velocity;
            }

            RotateTowardsMovementDir();


            newVelocity.y = rb.velocity.y;
            rb.velocity = newVelocity;
            //Return movement value for Animator component.
            return inputVec.magnitude;
        }

        #endregion

        #region Jumping

        /// <summary>
        /// Checks if character is within a certain distance from the ground, and markes it IsGrounded.
        /// </summary>
        void CheckForGrounded()
        {
            RaycastHit hit;
            Vector3 offset = new Vector3(0, 0.1f, 0);
            if (Physics.Raycast((transform.position + offset), -Vector3.up, out hit, 100f))
            {
                distanceToGround = hit.distance;
                if (distanceToGround < slopeAmount)
                {
                    isGrounded = true;
                    if (!isJumping)
                    {
                        canJump = true;
                    }
                    startFall = false;

                    isFalling = false;
                    fallTimer = 0;
                    if (!isJumping)
                    {
                        animator.SetInteger("Jumping", 0);
                    }
                }
                else
                {
                    fallTimer += 0.009f;
                    if (fallTimer >= fallDelay)
                    {
                        isGrounded = false;
                    }
                }
            }
        }

        void Jumping()
        {
            if (isGrounded)
            {
                if (canJump && inputJump)
                {
                    StartCoroutine(_Jump());
                }
            }
            else
            {
                canJump = false;
                if (isFalling)
                {
                    //Set the animation back to falling.
                    animator.SetInteger("Jumping", 2);
                    //Prevent from going into land animation while in air.
                    if (!startFall)
                    {
                        animator.SetTrigger("JumpTrigger");
                        startFall = true;
                    }
                }
            }
        }

        public IEnumerator _Jump()
        {
            isJumping = true;
            animator.SetInteger("Jumping", 1);
            animator.SetTrigger("JumpTrigger");
            //Apply the current movement to launch velocity.
            rb.velocity += jumpSpeed * Vector3.up;
            canJump = false;
            yield return new WaitForSeconds(.5f);
            isJumping = false;
        }

        /// <summary>
        /// Controls movement of character while in the air.
        /// </summary>
        void AirControl()
        {
            if (!isGrounded)
            {
                CameraRelativeMovement();
                Vector3 motion = inputVec;
                motion *= (Mathf.Abs(inputVec.x) == 1 && Mathf.Abs(inputVec.z) == 1) ? 0.7f : 1;
                rb.AddForce(motion * inAirSpeed, ForceMode.Acceleration);
                //Limit the amount of velocity character can achieve.
                float velocityX = 0;
                float velocityZ = 0;
                if (rb.velocity.x > maxVelocity)
                {
                    velocityX = GetComponent<Rigidbody>().velocity.x - maxVelocity;
                    if (velocityX < 0)
                    {
                        velocityX = 0;
                    }
                    rb.AddForce(new Vector3(-velocityX, 0, 0), ForceMode.Acceleration);
                }
                if (rb.velocity.x < minVelocity)
                {
                    velocityX = rb.velocity.x - minVelocity;
                    if (velocityX > 0)
                    {
                        velocityX = 0;
                    }
                    rb.AddForce(new Vector3(-velocityX, 0, 0), ForceMode.Acceleration);
                }
                if (rb.velocity.z > maxVelocity)
                {
                    velocityZ = rb.velocity.z - maxVelocity;
                    if (velocityZ < 0)
                    {
                        velocityZ = 0;
                    }
                    rb.AddForce(new Vector3(0, 0, -velocityZ), ForceMode.Acceleration);
                }
                if (rb.velocity.z < minVelocity)
                {
                    velocityZ = rb.velocity.z - minVelocity;
                    if (velocityZ > 0)
                    {
                        velocityZ = 0;
                    }
                    rb.AddForce(new Vector3(0, 0, -velocityZ), ForceMode.Acceleration);
                }
            }
        }

        #endregion

        #region Actions
        //0 = No side.
        //1 = Left.
        //2 = Right.
        public void Attack(int attackSide)
        {
            if (canAction && isGrounded)
            {
                if (pose == Pose.UNARMED)
                {
                    int maxAttacks = 3;
                    int attackNumber = 0;
                    if (attackSide == 1 || attackSide == 3)
                    {
                        attackNumber = Random.Range(3, maxAttacks);
                    }
                    else if (attackSide == 2)
                    {
                        attackNumber = Random.Range(6, maxAttacks + 3);
                    }
                    if (attackSide != 3)
                    {
                        animator.SetInteger("Action", attackNumber);
                        if (leftWeapon == 12 || leftWeapon == 14 || rightWeapon == 13 || rightWeapon == 15)
                        {
                            StartCoroutine(_LockMovementAndAttack(0, .75f));
                        }
                        else
                        {
                            StartCoroutine(_LockMovementAndAttack(0, .6f));
                        }
                    }
                    else
                    {
                        StartCoroutine(_LockMovementAndAttack(0, .75f));
                    }
                }
                animator.SetTrigger("AttackTrigger");
            }
        }

        public void AttackKick(int kickSide)
        {
            if (isGrounded)
            {
                animator.SetInteger("Action", kickSide);
                animator.SetTrigger("AttackKickTrigger");
                StartCoroutine(_LockMovementAndAttack(0, .8f));
            }
        }

        #endregion

        #region Targeting

        //public void Targeting()
        //{
        //    if (inputTriggerL)
        //        GameManager.instance.GetPreviousTarget();

        //    if (inputTriggerR)
        //        GameManager.instance.GetNextTarget();

        //    if (inputCancel)
        //        GameManager.instance.ClearTarget();
        //}
        
        #endregion

        #region Misc

        //Kkeep character from moveing while attacking, etc.
        public IEnumerator _LockMovementAndAttack(float delayTime, float lockTime)
        {
            yield return new WaitForSeconds(delayTime);
            canAction = false;
            canMove = false;
            animator.SetBool("Moving", false);
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            inputVec = new Vector3(0, 0, 0);
            animator.applyRootMotion = true;

            yield return new WaitForSeconds(lockTime);
            canAction = true;
            canMove = true;
            animator.applyRootMotion = false;
        }

        //Placeholder functions for Animation events.
        public void Hit()
        {
        }

        public void Shoot()
        {
        }

        public void FootR()
        {
        }

        public void FootL()
        {
        }

        public void Land()
        {
        }

        public void Jump()
        {
        }

        #endregion
    }
}