﻿using UnityEngine;

namespace Maelstrom.RPG
{
    
    //joystick-based camera rotator script
    public class CameraRotator : MonoBehaviour
    {
        #region Variables
        public Transform target;
        public float camSpeed = 2f;
        public float followDistance = 5f;

        private float smoothing;
        float maxHeight = 10f;
        float angle = 0f;
        float inputCamHorizontal = 0f;
        float inputCamVertical = 0f;
        float inputCamDistance = 0f;
        #endregion

        void Start()
        {
            Vector3 offset = target.transform.position - transform.position;
            followDistance = offset.magnitude;
            angle = -Vector3.Angle(Vector3.zero, offset);
        }

        void Update()
        {
            if (GameManager.GameIsPaused)
            {
                inputCamHorizontal = inputCamVertical = inputCamDistance = 0f;
            }
            else 
            {
                inputCamHorizontal = Input.GetAxisRaw("Horizontal R");
                inputCamVertical = Input.GetAxisRaw("Vertical R");
                inputCamDistance = Input.GetAxisRaw("Triggers");
            }


            smoothing = camSpeed / 10f;

            angle += inputCamHorizontal * camSpeed;
            if (angle >= 360)
                angle -= 360;
            else if (angle < 0)
                angle += 360;

            followDistance += inputCamDistance * smoothing;
            followDistance = Mathf.Clamp(followDistance, 5f, 15f);  //min and max follow distance

            var x = followDistance * Mathf.Cos(angle * Mathf.Deg2Rad);
            var y = transform.position.y;
            var z = followDistance * Mathf.Sin(angle * Mathf.Deg2Rad);

            y -= inputCamVertical * smoothing;
            y = Mathf.Clamp(y, .5f, maxHeight);  //min and max camera height

            Vector3 newPosition = new Vector3(target.transform.position.x + x, y, target.transform.position.z + z);
            transform.position = newPosition;

            transform.LookAt(target);
        }
    }

}