﻿using System.Collections;
using UnityEngine;

public enum WeaponPose
{
    UNARMED = 0,
    TWOHANDSWORD = 1,
    TWOHANDSPEAR = 2,
    TWOHANDAXE = 3,
    TWOHANDBOW = 4,
    TWOHANDCROSSBOW = 5,
    STAFF = 6,
    ARMED = 7,
    RELAX = 8,
    RIFLE = 9,
    TWOHANDCLUB = 10,
    SHIELD = 11,
    ARMEDSHIELD = 12
}

public enum RPGCharacterState
{
    DEFAULT,
    BLOCKING,
    STRAFING,
    CLIMBING,
    SWIMMING
}

namespace Maelstrom.RPG
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Animator))]
    public class PlayerAnimationController : MonoBehaviour
    {

        #region Variables

        //Components
        public Rigidbody rb;
        public Animator animator;
        ParticleSystem FXSplash;
        //public Camera sceneCamera;
        public Vector3 waistRotationOffset;
        public RPGCharacterState rpgCharacterState = RPGCharacterState.DEFAULT;

        //inputs
        bool inputPause;
        bool inputSelect;

        bool inputCancel;
        bool inputConfirm;

        float inputHorizontal;
        float inputVertical;

        //jumping variables
        public float gravity = -9.8f;
        [HideInInspector]
        public float gravityTemp = 0f;
        [HideInInspector]
        public bool canJump;
        bool isJumping = false;
        [HideInInspector]
        public bool isGrounded;
        public float jumpSpeed = 12;
        bool doJump = false;
        bool isFalling;
        bool startFall;
        float fallingVelocity = -1f;

        //movement variables
        [HideInInspector]
        public bool isMoving = false;
        [HideInInspector]
        public bool canMove = true;
        public float walkSpeed = 1.35f;
        float moveSpeed;
        public float runSpeed = 6f;
        float rotationSpeed = 40f;

        float x;
        float z;

        Vector3 inputVec;
        Vector3 newVelocity;

        //action variables
        [HideInInspector]
        bool canAction = true;
        [HideInInspector]
        bool isDead = false;
        [HideInInspector]
        bool isBlocking = false;
        public float blockTime = 1f;      
        [HideInInspector]
        public bool isCasting;

        //Weapon and Shield
        public WeaponPose weapon;
        [HideInInspector]
        public int rightWeapon = 0;
        [HideInInspector]
        public int leftWeapon = 0;
        [HideInInspector]
        public bool isRelax = false;
        bool isSwitchingFinished = true;

        //Weapon Models
        public GameObject[] weaponModels;
        public bool instantWeaponSwitch = false;

        #endregion


        void Awake()
        {
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody>();
            HideAllWeapons();

            Player player = GetComponentInParent<Player>();

            if (player.equipment.weapon)
            {
                weapon = GetPose(player.equipment.weapon.weaponType);
                StartCoroutine(UnsheatheWeapon((int)player.equipment.weapon.weaponType));
            }
            else
            {
                weapon = WeaponPose.UNARMED;
            }
        }

        //gets the appropriate pose for the weapon type
        WeaponPose GetPose(WeaponType weapon)
        {
            if (weapon == WeaponType.TwoHandAxe)
            {
                return WeaponPose.TWOHANDAXE;
            }
            if (weapon == WeaponType.TwoHandClub)
            {
                return WeaponPose.TWOHANDCLUB;
            }
            if (weapon == WeaponType.Bow)
            {
                return WeaponPose.TWOHANDBOW;
            }
            if (weapon == WeaponType.Crossbow)
            {
                return WeaponPose.TWOHANDCROSSBOW;
            }
            if (weapon == WeaponType.Staff)
            {
                return WeaponPose.STAFF;
            }
            if (weapon == WeaponType.Rifle)
            {
                return WeaponPose.RIFLE;
            }
            if (weapon == WeaponType.TwoHandSpear)
            {
                return WeaponPose.TWOHANDSPEAR;
            }
            if (weapon == WeaponType.TwoHandSword)
            {
                return WeaponPose.TWOHANDSWORD;
            }
            return WeaponPose.ARMED;
        }

        #region Fixed/Late Update

        //FixedUpdate() contains all the physics dependant functions.
        void FixedUpdate()
        {
            CheckForGrounded();
            //apply gravity force
            rb.AddForce(0, gravity, 0, ForceMode.Acceleration);
            
            //check if falling
            if (rb.velocity.y < fallingVelocity)
            {
                isFalling = true;
                animator.SetInteger("Jumping", 2);
                canJump = false;
            }
            else
            {
                isFalling = false;
            }
            
            moveSpeed = UpdateMovement();
        }

        //LateUpdate() handles all the calls to the Animator.
        //get velocity of rigid body and pass the value to the animator to control the animations
        void LateUpdate()
        {
            //Get local velocity of charcter
            float velocityXel = transform.InverseTransformDirection(rb.velocity).x;
            float velocityZel = transform.InverseTransformDirection(rb.velocity).z;
            //Update animator with movement values
            animator.SetFloat("Velocity X", velocityXel / runSpeed);
            animator.SetFloat("Velocity Z", velocityZel / runSpeed);
            //if character is alive and can move, set our animator
            if (!isDead && canMove)
            {
                if (moveSpeed > 0)
                {
                    animator.SetBool("Moving", true);
                    isMoving = true;
                }
                else
                {
                    animator.SetBool("Moving", false);
                    isMoving = false;
                }
            }            
        }
        #endregion

        #region Update Movement

        float UpdateMovement()
        {
            Vector3 motion = inputVec;
            if (isGrounded && rpgCharacterState != RPGCharacterState.CLIMBING)
            {
                //reduce input for diagonal movement
                if (motion.magnitude > 1)
                {
                    motion.Normalize();
                }
                if (canMove && !isBlocking)
                {
                    newVelocity = motion * runSpeed;
                }
            }
            else
            {
                if (rpgCharacterState != RPGCharacterState.SWIMMING)
                {
                    //if we are falling use momentum
                    newVelocity = rb.velocity;
                }
                else
                {
                    newVelocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
                }
            }

            if (canMove)
            {
                RotateTowardsMovementDir();
            }

            //if we are falling use momentum
            newVelocity.y = rb.velocity.y;
            rb.velocity = newVelocity;
            //return a movement value for the animator
            return inputVec.magnitude;
        }

        //rotate character towards direction moved
        void RotateTowardsMovementDir()
        {
            if (inputVec != Vector3.zero && !isBlocking && rpgCharacterState != RPGCharacterState.CLIMBING)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(inputVec), Time.deltaTime * rotationSpeed);
            }
        }

        #endregion

        #region Jumping
        void CheckForGrounded()
        {
            float distanceToGround;
            float threshold = .45f;
            RaycastHit hit;
            Vector3 offset = new Vector3(0, 0.4f, 0);
            if (Physics.Raycast((transform.position + offset), -Vector3.up, out hit, 100f))
            {
                distanceToGround = hit.distance;
                if (distanceToGround < threshold)
                {
                    isGrounded = true;
                    canJump = true;
                    startFall = false;
                    isFalling = false;
                    if (!isJumping)
                    {
                        animator.SetInteger("Jumping", 0);
                    }
                }
                else
                {
                    isGrounded = false;
                }
            }
        }

        void Jumping()
        {
            if (isGrounded)
            {
                if (canJump && doJump)
                {
                    StartCoroutine(Jump());
                }
            }
            else
            {
                canJump = false;
                if (isFalling)
                {
                    //set the animation back to falling
                    animator.SetInteger("Jumping", 2);
                    //prevent from going into land animation while in air
                    if (!startFall)
                    {
                        animator.SetTrigger("JumpTrigger");
                        startFall = true;
                    }
                }
            }
        }
        #endregion

        //start of battle = start empty handed and pull out weapons

        //cast animation = put away weapon & do cast animation
        //                 expand sacred geometric symbol corresponding to element & rotate
        //                 "levitate" clothing, while rays and particles shoot upwards
        //                 fade ground symbol
        //                 return to weapon out


        #region Animation Control

        void HideAllWeapons()
        {
            foreach (GameObject o in weaponModels)
            {
                if (o)
                    o.SetActive(false);
            }
        }

        public IEnumerator SheatheWeapon(int weaponNumber, int weaponDraw)
        {
            if ((weaponNumber == 8 || weaponNumber == 10 || weaponNumber == 12 || weaponNumber == 14 || weaponNumber == 16))
            {
                animator.SetInteger("LeftRight", 1);
            }
            else if ((weaponNumber == 9 || weaponNumber == 11 || weaponNumber == 13 || weaponNumber == 15 || weaponNumber == 17 || weaponNumber == 19))
            {
                animator.SetInteger("LeftRight", 2);
            }
            else if (weaponNumber == 7)
            {
                animator.SetInteger("LeftRight", 1);
            }
            //if switching to unarmed, don't set "Armed" until after 2nd weapon sheath
            if (weaponDraw == 0)
            {
                if (leftWeapon == 0 && rightWeapon != 0)
                {
                    animator.SetBool("Armed", false);
                }
                if (rightWeapon == 0 && leftWeapon != 0)
                {
                    animator.SetBool("Armed", false);
                }
            }
            if (!instantWeaponSwitch)
            {
                animator.SetTrigger("WeaponSheathTrigger");
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                animator.SetTrigger("InstantSwitchTrigger");
            }
            //Sheath 2 handed weapons
            if (weaponNumber < 7 || weaponNumber == 18 || weaponNumber == 19 || weaponNumber == 20)
            {
                leftWeapon = 0;
                animator.SetInteger("LeftWeapon", 0);
                rightWeapon = 0;
                animator.SetInteger("RightWeapon", 0);
                animator.SetBool("Shield", false);
                animator.SetBool("Armed", false);
            }
            //Sheath Shield
            else if (weaponNumber == 7)
            {
                leftWeapon = 0;
                animator.SetInteger("LeftWeapon", 0);
                animator.SetBool("Shield", false);
            }
            //Sheath left weapon
            else if ((weaponNumber == 8 || weaponNumber == 10 || weaponNumber == 12 || weaponNumber == 14 || weaponNumber == 16))
            {
                leftWeapon = 0;
                animator.SetInteger("LeftWeapon", 0);
            }
            //Sheath right weapon
            else if ((weaponNumber == 9 || weaponNumber == 11 || weaponNumber == 13 || weaponNumber == 15 || weaponNumber == 17 || weaponNumber == 19))
            {
                rightWeapon = 0;
                animator.SetInteger("RightWeapon", 0);
            }
            //if switched to unarmed
            if (leftWeapon == 0 && rightWeapon == 0)
            {
                animator.SetBool("Armed", false);
            }
            if (leftWeapon == 0 && rightWeapon == 0)
            {
                animator.SetInteger("LeftRight", 0);
                animator.SetInteger("Weapon", 0);
                animator.SetBool("Armed", false);
                weapon = WeaponPose.UNARMED;
            }
            if (instantWeaponSwitch)
            {
                StartCoroutine(WeaponVisibility(weaponNumber, 0f, false));
            }
            else
            {
                StartCoroutine(WeaponVisibility(weaponNumber, 0.4f, false));
            }
            yield return null;
        }

        public IEnumerator UnsheatheWeapon(int weaponNumber)
        {
            if (!instantWeaponSwitch)
            {
                animator.SetInteger("Weapon", -1);
            }
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            //two handed weapons
            if (weaponNumber < 7 || weaponNumber == 18 || weaponNumber == 20)
            {
                leftWeapon = weaponNumber;
                animator.SetInteger("LeftRight", 3);
                if (weaponNumber == 0)
                {
                    weapon = WeaponPose.UNARMED;
                }
                if (weaponNumber == 1)
                {
                    weapon = WeaponPose.TWOHANDSWORD;
                    if (!instantWeaponSwitch)
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.4f, true));
                    }
                }
                else if (weaponNumber == 2)
                {
                    weapon = WeaponPose.TWOHANDSPEAR;
                    if (!instantWeaponSwitch)
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.5f, true));
                    }
                }
                else if (weaponNumber == 3)
                {
                    weapon = WeaponPose.TWOHANDAXE;
                    if (!instantWeaponSwitch)
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.5f, true));
                    }
                }
                else if (weaponNumber == 4)
                {
                    weapon = WeaponPose.TWOHANDBOW;
                    if (!instantWeaponSwitch)
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.55f, true));
                    }
                }
                else if (weaponNumber == 5)
                {
                    weapon = WeaponPose.TWOHANDCROSSBOW;
                    if (!instantWeaponSwitch)
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.5f, true));
                    }
                }
                else if (weaponNumber == 6)
                {
                    weapon = WeaponPose.STAFF;
                    if (!instantWeaponSwitch)
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.6f, true));
                    }
                }
                else if (weaponNumber == 18)
                {
                    weapon = WeaponPose.RIFLE;
                    if (!instantWeaponSwitch)
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.6f, true));
                    }
                }
                else if (weaponNumber == 20)
                {
                    weapon = WeaponPose.TWOHANDCLUB;
                    if (!instantWeaponSwitch)
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.6f, true));
                    }
                }
                if (!instantWeaponSwitch)
                {
                    animator.SetTrigger("WeaponUnsheathTrigger");
                }
                if (instantWeaponSwitch)
                {
                    StartCoroutine(WeaponVisibility(weaponNumber, 0.2f, true));
                    animator.SetTrigger("InstantSwitchTrigger");
                }
            }
            //one handed weapons
            else
            {
                //shield
                if (weaponNumber == 7)
                {
                    leftWeapon = 7;
                    animator.SetInteger("LeftWeapon", 7);
                    animator.SetInteger("LeftRight", 1);
                    if (!instantWeaponSwitch)
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.6f, true));
                        animator.SetTrigger("WeaponUnsheathTrigger");
                    }
                    else
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.2f, true));
                        animator.SetTrigger("InstantSwitchTrigger");
                    }
                    animator.SetBool("Shield", true);
                }

                //left hand weapons
                else if (weaponNumber == 8 || weaponNumber == 10 || weaponNumber == 12 || weaponNumber == 14 || weaponNumber == 16)
                {
                    animator.SetInteger("LeftRight", 1);
                    animator.SetInteger("LeftWeapon", weaponNumber);
                    if (!instantWeaponSwitch)
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.6f, true));
                        animator.SetTrigger("WeaponUnsheathTrigger");
                    }
                    else
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.2f, true));
                        animator.SetTrigger("InstantSwitchTrigger");
                    }
                    leftWeapon = weaponNumber;
                    weaponNumber = 7;
                }

                //right hand weapons
                else if (weaponNumber == 9 || weaponNumber == 11 || weaponNumber == 13 || weaponNumber == 15 || weaponNumber == 17 || weaponNumber == 19)
                {
                    animator.SetInteger("LeftRight", 2);
                    animator.SetInteger("RightWeapon", weaponNumber);
                    rightWeapon = weaponNumber;
                    if (!instantWeaponSwitch)
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.6f, true));
                        animator.SetTrigger("WeaponUnsheathTrigger");
                    }
                    else
                    {
                        StartCoroutine(WeaponVisibility(weaponNumber, 0.2f, true));
                        animator.SetTrigger("InstantSwitchTrigger");
                    }
                    weaponNumber = 7;
                    //set shield to false for animator, will reset later
                    if (leftWeapon == 7)
                    {
                        animator.SetBool("Shield", false);
                    }
                }
            }
            if (weapon == WeaponPose.RIFLE)
            {
                animator.SetInteger("Weapon", 8);
            }
            else if (weapon == WeaponPose.TWOHANDCLUB)
            {
                animator.SetInteger("Weapon", 9);
            }
            else
            {
                animator.SetInteger("Weapon", weaponNumber);
            }
            if (leftWeapon == 7)
            {
                if (rightWeapon == 0)
                {
                    animator.SetBool("Shield", true);
                    weapon = WeaponPose.SHIELD;
                }
                else
                {
                    animator.SetBool("Shield", true);
                    weapon = WeaponPose.ARMEDSHIELD;
                }
                animator.SetBool("Shield", true);
            }
            if ((leftWeapon > 6 || rightWeapon > 6) && weapon != WeaponPose.RIFLE && weapon != WeaponPose.TWOHANDCLUB)
            {
                animator.SetBool("Armed", true);
                if (leftWeapon != 7)
                {
                    weapon = WeaponPose.ARMED;
                }
            }
            //For dual blocking
            if (rightWeapon == 9 || rightWeapon == 11 || rightWeapon == 13 || rightWeapon == 15 || rightWeapon == 17)
            {
                if (leftWeapon == 8 || leftWeapon == 10 || leftWeapon == 12 || leftWeapon == 14 || leftWeapon == 16)
                {
                    yield return new WaitForSeconds(.1f);
                    animator.SetInteger("LeftRight", 3);
                }
            }
            if (leftWeapon == 8 || leftWeapon == 10 || leftWeapon == 12 || leftWeapon == 14 || leftWeapon == 16)
            {
                if (rightWeapon == 9 || rightWeapon == 11 || rightWeapon == 13 || rightWeapon == 15 || rightWeapon == 17)
                {
                    yield return new WaitForSeconds(.1f);
                    animator.SetInteger("LeftRight", 3);
                }
            }
            if (instantWeaponSwitch)
            {
                animator.SetTrigger("InstantSwitchTrigger");
            }
            yield return null;
        }

        public IEnumerator WeaponVisibility(int weaponNumber, float delayTime, bool visible)
        {
            yield return new WaitForSeconds(delayTime);

            weaponModels[weaponNumber-1].SetActive(visible);
            yield return null;
        }

        public void Attack(int attackSide)
        {
            //No controller input
            if (inputVec.magnitude == 0f)
            {
                if (weapon == WeaponPose.UNARMED || weapon == WeaponPose.ARMED || weapon == WeaponPose.ARMEDSHIELD)
                {
                    int maxAttacks = 3;
                    int attackNumber = 0;
                    //left attacks
                    if (attackSide == 1)
                    {
                        animator.SetInteger("AttackSide", 1);
                        //Left sword has 6 attacks
                        if (leftWeapon == 8)
                        {
                            attackNumber = Random.Range(1, 6);
                        }
                        else
                        {
                            attackNumber = Random.Range(1, maxAttacks);
                        }
                    }
                    //right attacks
                    else if (attackSide == 2)
                    {
                        animator.SetInteger("AttackSide", 2);
                        //Right spear has 7 attacks
                        if (rightWeapon == 19)
                        {
                            attackNumber = Random.Range(1, 7);
                        }
                        //Right sword has 6 attacks
                        else if (rightWeapon == 9)
                        {
                            attackNumber = Random.Range(7, 12);
                        }
                        else
                        {
                            attackNumber = Random.Range(3, maxAttacks + 3);
                        }
                    }
                    //dual attacks
                    else if (attackSide == 3)
                    {
                        attackNumber = Random.Range(1, maxAttacks);
                    }
                    if (isGrounded)
                    {
                        if (attackSide != 3)
                        {
                            animator.SetTrigger("Attack" + (attackNumber + 1).ToString() + "Trigger");
                            if (leftWeapon == 12 || leftWeapon == 14 || rightWeapon == 13 || rightWeapon == 15 || rightWeapon == 19)
                            {
                                //StartCoroutine(_LockMovementAndAttack(0, 0.75f));
                            }
                            else
                            {
                                //StartCoroutine(_LockMovementAndAttack(0, 0.7f));
                            }
                        }
                        //Dual Attacks
                        else
                        {
                            animator.SetTrigger("AttackDual" + (attackNumber + 1).ToString() + "Trigger");
                            //StartCoroutine(_LockMovementAndAttack(0, 0.75f));
                        }
                    }
                }
                else if (weapon == WeaponPose.SHIELD)
                {
                    int maxAttacks = 1;
                    {
                        int attackNumber = Random.Range(1, maxAttacks);
                        if (isGrounded)
                        {
                            animator.SetTrigger("Attack" + (attackNumber).ToString() + "Trigger");
                            //StartCoroutine(_LockMovementAndAttack(0, 1.1f));
                        }
                    }
                }
                else if (weapon == WeaponPose.TWOHANDSPEAR)
                {
                    int maxAttacks = 10;
                    {
                        int attackNumber = Random.Range(1, maxAttacks);
                        if (isGrounded)
                        {
                            animator.SetTrigger("Attack" + (attackNumber).ToString() + "Trigger");
                            //StartCoroutine(_LockMovementAndAttack(0, 1.1f));
                        }
                    }
                }
                else if (weapon == WeaponPose.TWOHANDCLUB)
                {
                    int maxAttacks = 10;
                    {
                        int attackNumber = Random.Range(1, maxAttacks);
                        if (isGrounded)
                        {
                            animator.SetTrigger("Attack" + (attackNumber).ToString() + "Trigger");
                            //StartCoroutine(_LockMovementAndAttack(0, 1.1f));
                        }
                    }
                }
                else if (weapon == WeaponPose.TWOHANDSWORD)
                {
                    int maxAttacks = 11;
                    {
                        int attackNumber = Random.Range(1, maxAttacks);
                        if (isGrounded)
                        {
                            animator.SetTrigger("Attack" + (attackNumber).ToString() + "Trigger");
                            //StartCoroutine(_LockMovementAndAttack(0, 1.1f));
                        }
                    }
                }
                else
                {
                    int maxAttacks = 6;
                    {
                        int attackNumber = Random.Range(1, maxAttacks);
                        if (isGrounded)
                        {
                            animator.SetTrigger("Attack" + (attackNumber).ToString() + "Trigger");
                            if (weapon == WeaponPose.TWOHANDSWORD)
                            {
                                //StartCoroutine(_LockMovementAndAttack(0, 0.85f));
                            }
                            else if (weapon == WeaponPose.TWOHANDAXE)
                            {
                                //StartCoroutine(_LockMovementAndAttack(0, 1.5f));
                            }
                            else
                            {
                                //StartCoroutine(_LockMovementAndAttack(0, 0.75f));
                            }
                        }
                    }
                }
            }
            //Character is Moving, use running attacks.
            else if (weapon == WeaponPose.ARMED)
            {
                if (attackSide == 1)
                {
                    animator.SetTrigger("Attack1Trigger");
                }
                if (attackSide == 2)
                {
                    animator.SetTrigger("Attack4Trigger");
                }
                if (attackSide == 3)
                {
                    animator.SetTrigger("AttackDual1Trigger");
                }
            }
            else
            {
            }
        }

        public void Cast(int attackSide)
        {
            if (attackSide == 0)
            {
                animator.SetTrigger("CastEndTrigger");
                isCasting = false;                
                return;
            }
            if (weapon == WeaponPose.UNARMED || weapon == WeaponPose.STAFF || weapon == WeaponPose.ARMED)
            {
                int maxAttacks = 3;
                //Left
                if (attackSide == 1)
                {
                    int attackNumber = Random.Range(1, maxAttacks + 1);
                    if (isGrounded)
                    {
                        isCasting = true;
                        animator.SetTrigger("CastAttack" + (attackNumber).ToString() + "Trigger");                        
                    }
                }
                //Right
                if (attackSide == 2)
                {
                    int attackNumber = Random.Range(3, maxAttacks + 4);
                    if (isGrounded)
                    {
                        isCasting = true;
                        animator.SetTrigger("CastAttack" + (attackNumber).ToString() + "Trigger");                        
                    }
                }
                //Dual
                if (attackSide == 3)
                {
                    int attackNumber = Random.Range(1, maxAttacks + 1);
                    if (isGrounded)
                    {
                        if (rightWeapon == 0 && leftWeapon == 0)
                        {
                            isCasting = true;
                            animator.SetTrigger("CastDualAttack" + (attackNumber).ToString() + "Trigger");                            
                        }
                    }
                }
            }
        }


        public IEnumerator Jump()
        {
            isJumping = true;
            animator.SetInteger("Jumping", 1);
            animator.SetTrigger("JumpTrigger");
            // Apply the current movement to launch velocity
            rb.velocity += jumpSpeed * Vector3.up;
            canJump = false;
            yield return new WaitForSeconds(0.5f);
            isJumping = false;
        }

        public IEnumerator Block()
        {
            if (!isBlocking)
            {
                animator.SetTrigger("BlockTrigger");
            }
            isBlocking = true;
            canJump = false;
            animator.SetBool("Blocking", true);
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            inputVec = Vector3.zero;

            yield return new WaitForSeconds(blockTime);

            isBlocking = false;
            canJump = true;
            animator.SetBool("Blocking", false);            
        }

        public IEnumerator BlockHitReact()
        {
            int hits = 2;
            int hitNumber = Random.Range(0, hits);
            animator.SetTrigger("BlockGetHit" + (hitNumber + 1).ToString() + "Trigger");
            yield return null;
        }

        public IEnumerator GetHit()
        {
            if (weapon != WeaponPose.RIFLE)
            {
                int hits = 2; // 2 knockback animations
                int hitNumber = Random.Range(0, hits); //pick a random one
                animator.SetTrigger("GetHit" + (hitNumber + 1).ToString() + "Trigger");
            }
            else
            {
                animator.SetTrigger("GetHit1Trigger");
            }

            yield return new WaitForSeconds(0.4f);
        }

        public IEnumerator Death()
        {
            animator.SetTrigger("Death1Trigger");
            isDead = true;
            animator.SetBool("Moving", false);
            inputVec = new Vector3(0, 0, 0);
            yield return null;
        }

        public IEnumerator Revive()
        {
            animator.SetTrigger("Revive1Trigger");
            isDead = false;
            yield return null;
        }

        #endregion

        #region Animation Events
        //Placeholder functions for Animation events
        public void Hit()
        {
        }

        public void Shoot()
        {
        }

        public void FootR()
        {
        }

        public void FootL()
        {
        }

        public void Land()
        {
        }

        public void WeaponSwitch()
        {
        }

        #endregion


    }
}