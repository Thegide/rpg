﻿using System.Collections;
using UnityEngine;


namespace Maelstrom.RPG
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Animator))]

    public class EnemyAnimationController : MonoBehaviour
    {

        #region Variables

        public Rigidbody rb;
        public Animator animator;
        public Vector3 waistRotationOffset;
        public RPGCharacterState rpgCharacterState = RPGCharacterState.DEFAULT;        

        //jumping variables
        public float gravity = -9.8f;
        [HideInInspector]
        public float gravityTemp = 0f;
        [HideInInspector]
        public bool canJump;
        bool isJumping = false;
        [HideInInspector]
        public bool isGrounded;
        public float jumpSpeed = 12;
        bool doJump = false;
        bool isFalling;
        bool startFall;
        float fallingVelocity = -1f;

        //movement variables
        [HideInInspector]
        public bool isMoving = false;
        [HideInInspector]
        public bool canMove = true;
        public float walkSpeed = 1.35f;
        float moveSpeed;
        public float runSpeed = 6f;
        float rotationSpeed = 40f;

        float x;
        float z;

        Vector3 inputVec;
        Vector3 newVelocity;

        //action variables
        [HideInInspector]
        bool isDead = false;
        [HideInInspector]
        bool isBlocking = false;
        public float blockTime = 1f;
        [HideInInspector]
        public bool isCasting;

        //Weapon and Shield
        public GameObject weaponModel;
        public WeaponPose weapon;
        [HideInInspector]
        public int rightWeapon = 9;
        [HideInInspector]
        public int leftWeapon = 0;
        [HideInInspector]
        public bool isRelax = false;
        bool isSwitchingFinished = true;


        #endregion

        void Awake()
        {
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody>();

            if (weaponModel)
                weaponModel.SetActive(false);

            StartCoroutine(UnsheatheWeapon((int)WeaponType.RSword));
        }
        
        #region Fixed/Late Update

        //FixedUpdate() contains all the physics dependant functions.
        void FixedUpdate()
        {
            CheckForGrounded();
            //apply gravity force
            rb.AddForce(0, gravity, 0, ForceMode.Acceleration);

            //check if falling
            if (rb.velocity.y < fallingVelocity)
            {
                isFalling = true;
                animator.SetInteger("Jumping", 2);
                canJump = false;
            }
            else
            {
                isFalling = false;
            }

            moveSpeed = UpdateMovement();
        }

        //LateUpdate() handles all the calls to the Animator.
        //get velocity of rigid body and pass the value to the animator to control the animations
        void LateUpdate()
        {
            //Get local velocity of charcter
            float velocityXel = transform.InverseTransformDirection(rb.velocity).x;
            float velocityZel = transform.InverseTransformDirection(rb.velocity).z;
            //Update animator with movement values
            animator.SetFloat("Velocity X", velocityXel / runSpeed);
            animator.SetFloat("Velocity Z", velocityZel / runSpeed);
            //if character is alive and can move, set our animator
            if (!isDead && canMove)
            {
                if (moveSpeed > 0)
                {
                    animator.SetBool("Moving", true);
                    isMoving = true;
                }
                else
                {
                    animator.SetBool("Moving", false);
                    isMoving = false;
                }
            }
        }
        #endregion

        #region Update Movement

        float UpdateMovement()
        {
            Vector3 motion = inputVec;
            if (isGrounded && rpgCharacterState != RPGCharacterState.CLIMBING)
            {
                //reduce input for diagonal movement
                if (motion.magnitude > 1)
                {
                    motion.Normalize();
                }
                if (canMove && !isBlocking)
                {
                    newVelocity = motion * runSpeed;
                }
            }
            else
            {
                if (rpgCharacterState != RPGCharacterState.SWIMMING)
                {
                    //if we are falling use momentum
                    newVelocity = rb.velocity;
                }
                else
                {
                    newVelocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
                }
            }

            if (canMove)
            {
                RotateTowardsMovementDir();
            }

            //if we are falling use momentum
            newVelocity.y = rb.velocity.y;
            rb.velocity = newVelocity;
            //return a movement value for the animator
            return inputVec.magnitude;
        }

        //rotate character towards direction moved
        void RotateTowardsMovementDir()
        {
            if (inputVec != Vector3.zero && !isBlocking && rpgCharacterState != RPGCharacterState.CLIMBING)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(inputVec), Time.deltaTime * rotationSpeed);
            }
        }

        #endregion

        #region Jumping
        void CheckForGrounded()
        {
            float distanceToGround;
            float threshold = .45f;
            RaycastHit hit;
            Vector3 offset = new Vector3(0, 0.4f, 0);
            if (Physics.Raycast((transform.position + offset), -Vector3.up, out hit, 100f))
            {
                distanceToGround = hit.distance;
                if (distanceToGround < threshold)
                {
                    isGrounded = true;
                    canJump = true;
                    startFall = false;
                    isFalling = false;
                    if (!isJumping)
                    {
                        animator.SetInteger("Jumping", 0);
                    }
                }
                else
                {
                    isGrounded = false;
                }
            }
        }

        void Jumping()
        {
            if (isGrounded)
            {
                if (canJump && doJump)
                {
                    StartCoroutine(Jump());
                }
            }
            else
            {
                canJump = false;
                if (isFalling)
                {
                    //set the animation back to falling
                    animator.SetInteger("Jumping", 2);
                    //prevent from going into land animation while in air
                    if (!startFall)
                    {
                        animator.SetTrigger("JumpTrigger");
                        startFall = true;
                    }
                }
            }
        }
        #endregion

        #region Animation Control

        public IEnumerator UnsheatheWeapon(int weaponNumber)
        {
            animator.SetInteger("Weapon", -1);

            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            //two handed weapons
            if (weaponNumber < 7 || weaponNumber == 18 || weaponNumber == 20)
            {
                leftWeapon = weaponNumber;
                animator.SetInteger("LeftRight", 3);
                if (weaponNumber == 0)
                {
                    weapon = WeaponPose.UNARMED;
                }
                if (weaponNumber == 1)
                {
                    weapon = WeaponPose.TWOHANDSWORD;
                    StartCoroutine(WeaponVisibility(0.4f, true));
                }
                else if (weaponNumber == 2)
                {
                    weapon = WeaponPose.TWOHANDSPEAR;
                    StartCoroutine(WeaponVisibility(0.5f, true));
                }
                else if (weaponNumber == 3)
                {
                    weapon = WeaponPose.TWOHANDAXE;
                    StartCoroutine(WeaponVisibility(0.5f, true));
                }
                else if (weaponNumber == 4)
                {
                    weapon = WeaponPose.TWOHANDBOW;
                    StartCoroutine(WeaponVisibility(0.55f, true));
                }
                else if (weaponNumber == 5)
                {
                    weapon = WeaponPose.TWOHANDCROSSBOW;
                    StartCoroutine(WeaponVisibility(0.5f, true));
                }
                else if (weaponNumber == 6)
                {
                    weapon = WeaponPose.STAFF;
                    StartCoroutine(WeaponVisibility(0.6f, true));
                }
                else if (weaponNumber == 18)
                {
                    weapon = WeaponPose.RIFLE;
                    StartCoroutine(WeaponVisibility(0.6f, true));
                }
                else if (weaponNumber == 20)
                {
                    weapon = WeaponPose.TWOHANDCLUB;
                    StartCoroutine(WeaponVisibility(0.6f, true));
                }

                animator.SetTrigger("WeaponUnsheathTrigger");                
            }
            //one handed weapons
            else
            {
                //shield
                if (weaponNumber == 7)
                {
                    leftWeapon = 7;
                    animator.SetInteger("LeftWeapon", 7);
                    animator.SetInteger("LeftRight", 1);
                    StartCoroutine(WeaponVisibility(0.6f, true));
                    animator.SetTrigger("WeaponUnsheathTrigger");                    
                    animator.SetBool("Shield", true);
                }

                //left hand weapons
                else if (weaponNumber == 8 || weaponNumber == 10 || weaponNumber == 12 || weaponNumber == 14 || weaponNumber == 16)
                {
                    animator.SetInteger("LeftRight", 1);
                    animator.SetInteger("LeftWeapon", weaponNumber);
                    StartCoroutine(WeaponVisibility(0.6f, true));
                    animator.SetTrigger("WeaponUnsheathTrigger");
                    leftWeapon = weaponNumber;
                    weaponNumber = 7;
                }

                //right hand weapons
                else if (weaponNumber == 9 || weaponNumber == 11 || weaponNumber == 13 || weaponNumber == 15 || weaponNumber == 17 || weaponNumber == 19)
                {
                    animator.SetInteger("LeftRight", 2);
                    animator.SetInteger("RightWeapon", weaponNumber);
                    rightWeapon = weaponNumber;
                    StartCoroutine(WeaponVisibility(0.6f, true));
                    animator.SetTrigger("WeaponUnsheathTrigger");
                    weaponNumber = 7;
                    //set shield to false for animator, will reset later
                    if (leftWeapon == 7)
                    {
                        animator.SetBool("Shield", false);
                    }
                }
            }
            if (weapon == WeaponPose.RIFLE)
            {
                animator.SetInteger("Weapon", 8);
            }
            else if (weapon == WeaponPose.TWOHANDCLUB)
            {
                animator.SetInteger("Weapon", 9);
            }
            else
            {
                animator.SetInteger("Weapon", weaponNumber);
            }
            if (leftWeapon == 7)
            {
                if (rightWeapon == 0)
                {
                    animator.SetBool("Shield", true);
                    weapon = WeaponPose.SHIELD;
                }
                else
                {
                    animator.SetBool("Shield", true);
                    weapon = WeaponPose.ARMEDSHIELD;
                }
                animator.SetBool("Shield", true);
            }
            if ((leftWeapon > 6 || rightWeapon > 6) && weapon != WeaponPose.RIFLE && weapon != WeaponPose.TWOHANDCLUB)
            {
                animator.SetBool("Armed", true);
                if (leftWeapon != 7)
                {
                    weapon = WeaponPose.ARMED;
                }
            }
            //For dual blocking
            if (rightWeapon == 9 || rightWeapon == 11 || rightWeapon == 13 || rightWeapon == 15 || rightWeapon == 17)
            {
                if (leftWeapon == 8 || leftWeapon == 10 || leftWeapon == 12 || leftWeapon == 14 || leftWeapon == 16)
                {
                    yield return new WaitForSeconds(.1f);
                    animator.SetInteger("LeftRight", 3);
                }
            }
            if (leftWeapon == 8 || leftWeapon == 10 || leftWeapon == 12 || leftWeapon == 14 || leftWeapon == 16)
            {
                if (rightWeapon == 9 || rightWeapon == 11 || rightWeapon == 13 || rightWeapon == 15 || rightWeapon == 17)
                {
                    yield return new WaitForSeconds(.1f);
                    animator.SetInteger("LeftRight", 3);
                }
            }            
            yield return null;
        }

        public IEnumerator WeaponVisibility(float delayTime, bool visible)
        {
            if (weaponModel != null)
            {
                yield return new WaitForSeconds(delayTime);
                weaponModel.SetActive(visible);
            }
            yield return null;
        }

        public void Attack(int attackSide)
        {
            //No controller input
            if (inputVec.magnitude == 0f)
            {
                if (weapon == WeaponPose.UNARMED || weapon == WeaponPose.ARMED || weapon == WeaponPose.ARMEDSHIELD)
                {
                    int maxAttacks = 3;
                    int attackNumber = 0;
                    //left attacks
                    if (attackSide == 1)
                    {
                        animator.SetInteger("AttackSide", 1);
                        //Left sword has 6 attacks
                        if (leftWeapon == 8)
                        {
                            attackNumber = Random.Range(1, 6);
                        }
                        else
                        {
                            attackNumber = Random.Range(1, maxAttacks);
                        }
                    }
                    //right attacks
                    else if (attackSide == 2)
                    {
                        animator.SetInteger("AttackSide", 2);
                        //Right spear has 7 attacks
                        if (rightWeapon == 19)
                        {
                            attackNumber = Random.Range(1, 7);
                        }
                        //Right sword has 6 attacks
                        else if (rightWeapon == 9)
                        {
                            attackNumber = Random.Range(7, 12);
                        }
                        else
                        {
                            attackNumber = Random.Range(3, maxAttacks + 3);
                        }
                    }
                    //dual attacks
                    else if (attackSide == 3)
                    {
                        attackNumber = Random.Range(1, maxAttacks);
                    }
                    if (isGrounded)
                    {
                        if (attackSide != 3)
                        {
                            animator.SetTrigger("Attack" + (attackNumber + 1).ToString() + "Trigger");
                        }
                        //Dual Attacks
                        else
                        {
                            animator.SetTrigger("AttackDual" + (attackNumber + 1).ToString() + "Trigger");
                        }
                    }
                }
                else if (weapon == WeaponPose.SHIELD)
                {
                    int maxAttacks = 1;
                    {
                        int attackNumber = Random.Range(1, maxAttacks);
                        if (isGrounded)
                        {
                            animator.SetTrigger("Attack" + (attackNumber).ToString() + "Trigger");
                        }
                    }
                }
                else if (weapon == WeaponPose.TWOHANDSPEAR)
                {
                    int maxAttacks = 10;
                    {
                        int attackNumber = Random.Range(1, maxAttacks);
                        if (isGrounded)
                        {
                            animator.SetTrigger("Attack" + (attackNumber).ToString() + "Trigger");
                        }
                    }
                }
                else if (weapon == WeaponPose.TWOHANDCLUB)
                {
                    int maxAttacks = 10;
                    {
                        int attackNumber = Random.Range(1, maxAttacks);
                        if (isGrounded)
                        {
                            animator.SetTrigger("Attack" + (attackNumber).ToString() + "Trigger");                            
                        }
                    }
                }
                else if (weapon == WeaponPose.TWOHANDSWORD)
                {
                    int maxAttacks = 11;
                    {
                        int attackNumber = Random.Range(1, maxAttacks);
                        if (isGrounded)
                        {
                            animator.SetTrigger("Attack" + (attackNumber).ToString() + "Trigger");                            
                        }
                    }
                }
                else
                {
                    int maxAttacks = 6;
                    {
                        int attackNumber = Random.Range(1, maxAttacks);
                        if (isGrounded)
                        {
                            animator.SetTrigger("Attack" + (attackNumber).ToString() + "Trigger");
                        }
                    }
                }
            }
            //Character is Moving, use running attacks.
            else if (weapon == WeaponPose.ARMED)
            {
                if (attackSide == 1)
                {
                    animator.SetTrigger("Attack1Trigger");
                }
                if (attackSide == 2)
                {
                    animator.SetTrigger("Attack4Trigger");
                }
                if (attackSide == 3)
                {
                    animator.SetTrigger("AttackDual1Trigger");
                }
            }
            else
            {
            }
        }

        public void Cast(int attackSide)
        {
            if (attackSide == 0)
            {
                animator.SetTrigger("CastEndTrigger");
                isCasting = false;
                return;
            }
            if (weapon == WeaponPose.UNARMED || weapon == WeaponPose.STAFF || weapon == WeaponPose.ARMED)
            {
                int maxAttacks = 3;
                //Left
                if (attackSide == 1)
                {
                    int attackNumber = Random.Range(1, maxAttacks + 1);
                    if (isGrounded)
                    {
                        isCasting = true;
                        animator.SetTrigger("CastAttack" + (attackNumber).ToString() + "Trigger");
                    }
                }
                //Right
                if (attackSide == 2)
                {
                    int attackNumber = Random.Range(3, maxAttacks + 4);
                    if (isGrounded)
                    {
                        isCasting = true;
                        animator.SetTrigger("CastAttack" + (attackNumber).ToString() + "Trigger");
                    }
                }
                //Dual
                if (attackSide == 3)
                {
                    int attackNumber = Random.Range(1, maxAttacks + 1);
                    if (isGrounded)
                    {
                        if (rightWeapon == 0 && leftWeapon == 0)
                        {
                            isCasting = true;
                            animator.SetTrigger("CastDualAttack" + (attackNumber).ToString() + "Trigger");
                        }
                    }
                }
            }
        }

        public IEnumerator Jump()
        {
            isJumping = true;
            animator.SetInteger("Jumping", 1);
            animator.SetTrigger("JumpTrigger");
            // Apply the current movement to launch velocity
            rb.velocity += jumpSpeed * Vector3.up;
            canJump = false;
            yield return new WaitForSeconds(0.5f);
            isJumping = false;
        }

        public IEnumerator Block()
        {
            if (!isBlocking)
            {
                animator.SetTrigger("BlockTrigger");
            }
            isBlocking = true;
            canJump = false;
            animator.SetBool("Blocking", true);
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            inputVec = Vector3.zero;

            yield return new WaitForSeconds(blockTime);

            isBlocking = false;
            canJump = true;
            animator.SetBool("Blocking", false);
        }

        public IEnumerator BlockHitReact()
        {
            int hits = 2;
            int hitNumber = Random.Range(0, hits);
            animator.SetTrigger("BlockGetHit" + (hitNumber + 1).ToString() + "Trigger");
            yield return null;
        }

        public IEnumerator GetHit()
        {
            if (weapon != WeaponPose.RIFLE)
            {
                int hits = 2; // 2 knockback animations
                int hitNumber = Random.Range(0, hits); //pick a random one
                animator.SetTrigger("GetHit" + (hitNumber + 1).ToString() + "Trigger");
            }
            else
            {
                animator.SetTrigger("GetHit1Trigger");
            }

            yield return new WaitForSeconds(0.4f);
        }

        public IEnumerator Death()
        {
            animator.SetTrigger("Death1Trigger");
            isDead = true;
            animator.SetBool("Moving", false);
            inputVec = new Vector3(0, 0, 0);
            yield return null;
        }

        public IEnumerator Revive()
        {
            animator.SetTrigger("Revive1Trigger");
            isDead = false;
            yield return null;
        }

        #endregion

        #region Animation Events
        //Placeholder functions for Animation events
        public void Hit()
        {
        }

        public void Shoot()
        {
        }

        public void FootR()
        {
        }

        public void FootL()
        {
        }

        public void Land()
        {
        }

        public void WeaponSwitch()
        {
        }

        #endregion
    }
}
