﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Maelstrom.EventSystems;

namespace Maelstrom.RPG
{
    [RequireComponent (typeof(AudioSource))]
    public class DynamicWeatherController : MonoBehaviour
    {
        #region Variables
        public enum WeatherStates
        {
            Pick,
            Sunny,
            Overcast,
            Rain,
            Thunder,
            Fog,
            Snow
        }

        private WeatherStates _weatherState;
        private int _switchWeather;
        private float _switchWeatherTimer = 0f;   //switch weather time equals zero
        private float _resetWeatherTimer = 20f;    //defines value to reset weather timer to

        //Audio control
        private AudioSource audioSource;
        public float audioFadeTime = 0.25f;
        public AudioClip sunnyAudio;
        public AudioClip overcastAudio;
        public AudioClip rainAudio;
        public AudioClip thunderAudio;
        public AudioClip fogAudio;
        public AudioClip snowAudio;

        //Particle systems
        public ParticleSystem sunCloudsParticleSystem;
        public ParticleSystem cloudParticleSystem;
        public ParticleSystem rainParticleSystem;
        public ParticleSystem thunderParticleSystem;
        public ParticleSystem fogParticleSystem;
        public ParticleSystem snowParticleSystem;

        //Ambient Light variables
        public float lightTransitionTime = 0.1f;   //defines rate for light change

        public float sunnyLightIntensity = 1f;
        public float overcastIntensity = 0.8f;
        public float rainIntensity = 0.5f;
        public float thunderIntensity = 0.1f;
        public float fogIntensity = 0.6f;
        public float snowIntensity = 0.75f;

        //Fog variables
        public Color sunFog;
        public Color overcastFog;
        public Color rainFog;
        public Color thunderFog;
        public Color fogFog;
        public Color snowFog;

        public float fogChangeSpeed = 0.1f;


        
        
        
        //    public Material[] SkyBoxes;

        #endregion

        //private UnityAction weatherListener;

        #region Main Methods    
        void Start()
        {
            //weatherListener = new UnityAction(ChangeWeather);

            audioSource = GetComponent<AudioSource>();

            StartCoroutine(WeatherFSM());
        }

        void Update()
        {
            SwitchWeatherTimer(); //updates timer function

        }

        void SwitchWeatherTimer()
        {
            Debug.Log("SwitchWeatherTimer");
            _switchWeatherTimer -= Time.deltaTime;

            if (_switchWeatherTimer > 0)
            {
                return;
            }
            else
            {
                _weatherState = WeatherStates.Pick;
                _switchWeatherTimer = _resetWeatherTimer;
            }
        }
        #endregion

        #region Weather Methods

        IEnumerator WeatherFSM()
        {
            while (true)  //while the weather state machine is active switch the weather
            {
                switch (_weatherState)
                {
                    case WeatherStates.Pick:
                        {
                            PickWeather();
                            break;
                        }
                    case WeatherStates.Sunny:
                        {
                            SunnyWeather();
                            break;
                        }
                    case WeatherStates.Overcast:
                        {
                            OvercastWeather();
                            break;
                        }
                    case WeatherStates.Rain:
                        {
                            RainWeather();
                            break;
                        }
                    case WeatherStates.Thunder:
                        {
                            ThunderWeather();
                            break;
                        }
                    case WeatherStates.Fog:
                        {
                            FogWeather();
                            break;
                        }
                    case WeatherStates.Snow:
                        {
                            SnowWeather();
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }

                yield return null;
            }
        }

        void PickWeather()
        {
            Debug.Log("PickWeather");

            DisableParticleSystems();

            _switchWeather = Random.Range(0, 6); //upper bound is number of weather states

            switch (_switchWeather)
            {
                case 0:
                    _weatherState = WeatherStates.Sunny;
                    break;
                case 1:
                    _weatherState = WeatherStates.Overcast;
                    break;
                case 2:
                    _weatherState = WeatherStates.Rain;
                    break;
                case 3:
                    _weatherState = WeatherStates.Thunder;
                    break;
                case 4:
                    _weatherState = WeatherStates.Fog;
                    break;
                case 5:
                    _weatherState = WeatherStates.Snow;
                    break;
            }
        }

        void SunnyWeather()
        {
            Debug.Log("SunnyWeather");

            var em = sunCloudsParticleSystem.emission;
            em.enabled = true;

            AdjustLightIntensity(sunnyLightIntensity);
            CrossFadeAudio(sunnyAudio);
        }

        void OvercastWeather()
        {
            Debug.Log("OvercastWeather");

            var em = cloudParticleSystem.emission;
            em.enabled = true;

            AdjustLightIntensity(overcastIntensity);
            CrossFadeAudio(overcastAudio);
        }

        void RainWeather()
        {
            Debug.Log("RainWeather");

            var em = rainParticleSystem.emission;
            em.enabled = true;

            AdjustLightIntensity(rainIntensity);
            CrossFadeAudio(rainAudio);
        }

        void ThunderWeather()
        {
            Debug.Log("ThunderWeather");

            var em = thunderParticleSystem.emission;
            em.enabled = true;

            AdjustLightIntensity(thunderIntensity);
            CrossFadeAudio(thunderAudio);
        }

        void FogWeather()
        {
            Debug.Log("FogWeather");

            var em = fogParticleSystem.emission;
            em.enabled = true;

            AdjustLightIntensity(fogIntensity);
        }

        void SnowWeather()
        {
            Debug.Log("SnowWeather");

            var em = snowParticleSystem.emission;
            em.enabled = true;

            AdjustLightIntensity(snowIntensity);
            CrossFadeAudio(snowAudio);
        }

        #endregion


        #region Helper Methods

        void DisableParticleSystems()
        {
            ParticleSystem.EmissionModule[] emission = new ParticleSystem.EmissionModule[6];

            emission[0] = sunCloudsParticleSystem.emission;
            emission[1] = cloudParticleSystem.emission;
            emission[2] = rainParticleSystem.emission;
            emission[3] = thunderParticleSystem.emission;
            emission[4] = fogParticleSystem.emission;
            emission[5] = snowParticleSystem.emission;

            for (int i = 0; i < emission.Length; i++)
            {
                emission[i].enabled = false;
            }
        }

        void AdjustLightIntensity(float lightIntensity)
        {
            Light ambientLight = GetComponent<Light>();
            if (ambientLight.intensity > lightIntensity)
            {
                ambientLight.intensity -= lightTransitionTime * Time.deltaTime;
            }

            else if (ambientLight.intensity < lightIntensity)
            {
                ambientLight.intensity += lightTransitionTime * Time.deltaTime;
            }
        }

        void CrossFadeAudio(AudioClip clip)
        {
            if (clip != null)
            {
                //fade out the previous clip, if it is playing
                if (audioSource.volume > 0 && audioSource.clip != clip)
                {
                    audioSource.volume -= audioFadeTime * Time.deltaTime;
                }

                //change clips
                if (audioSource.volume == 0) 
                {
                    audioSource.Stop();
                    audioSource.clip = clip;
                    audioSource.loop = true;
                    audioSource.Play();
                }

                //fade in the current clip, if it is playing
                if (audioSource.volume < 1 && audioSource.clip == clip)
                {
                    audioSource.volume += audioFadeTime * Time.deltaTime;
                }
            }
        }

        #endregion



        void SetRainWeather()
        {
            //RenderSettings.skybox = SkyBoxes[(int)Weather.RAIN];
            //RainModule.SetActive(true);
            //DynamicGI.UpdateEnvironment();
        }





        #region Event Handling
        public void OnEnable()
        {
            //EventManager.StartListening("Weather", weatherListener);
        }

        public void OnDisable()
        {
            //EventManager.StopListening("Weather", weatherListener);
        }
        #endregion
    }
}