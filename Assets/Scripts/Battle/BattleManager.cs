﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Maelstrom.EventSystems;
using Maelstrom.RPG.UI;

namespace Maelstrom.RPG
{
    public class BattleManager : MonoBehaviour
    {
        public static BattleManager instance = null;

        #region Variables
        const int maxEnemies = 4;

        [HideInInspector]
        public Canvas mainCanvas;
        //public GameObject commandMenu;
        public GameObject gameOverPanel, victoryPanel; //will upgrade to single Dynamic UI panel later that is instantiated

        public enum BattleType { WAIT, ACTIVE };
        public BattleType battleType = BattleType.WAIT;

        public List<Player> players;
        public List<Enemy> enemies;

        public enum BattleState { ONGOING, IDLE };
        //[HideInInspector]
        public BattleState battleState;

        public List<Character> turnQueue = new List<Character>();
        private UnityAction queueLockListener, queueUnlockListener;
        public bool QueueIsLocked = false;
        public bool IsPaused = false;

        //Prefabs for battle initialization
        public GameObject enemyPrefab;
        public GameObject entityContainer;

        //Inputs
        public bool inputPause;

        #endregion

        #region Main Methods
        //setup for singleton
        void Awake()
        {
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);

            mainCanvas = FindObjectOfType<Canvas>();
            if (!mainCanvas)
            {
                Debug.LogError("Main Canvas not found.  Did you forget to create it?");
            }
            gameOverPanel.SetActive(false);


            queueLockListener = new UnityAction(LockQueue); //delegate method
            queueUnlockListener = new UnityAction(UnlockQueue); //delegate method
        }

        void Start()
        {
            //build references to all players in scene
            players = new List<Player>();
            enemies = new List<Enemy>();
            var playerObjects = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject o in playerObjects)
            {
                players.Add(o.GetComponent<Player>());
            }

            //add enemies to the scene

            //randomize the number of enemies (1..4)            
            int enemyCount = Random.Range(1, maxEnemies + 1);
            Debug.Log("Enemy count: " + enemyCount);

            //randomize the type of enemies
            //???

            Object[] enemyDatabase = Resources.LoadAll("Character Data/Enemy Data", typeof(Stats));
            Debug.Log(enemyDatabase.Length + " found in enemy database");

            for (int i = 0; i < enemyCount; i++)
            {
                //instantiate
                GameObject newEnemy = Instantiate(enemyPrefab, entityContainer.transform);
                newEnemy.SetActive(true);
                Enemy e = newEnemy.GetComponent<Enemy>();
                e.Stats = enemyDatabase[Random.Range(0, enemyDatabase.Length)] as Stats;
                e.Drops = Resources.Load("Character Data/Enemy Data/Drop Tables/" + e.Stats.name) as DropTable;
                e.Name = e.Stats.name + i;
                e.HP = e.Stats.maxHP;
                e.MP = e.Stats.maxMP;

                enemies.Add(e);
            }
                        
            //pattern distribution function - eventually replace with different layouts and populate
            float centerX = 5f;
            float centerZ = 0f;
            float offsetZ = 3f;
            
            for (int i = 0; i < enemies.Count; i++)
            {
                Vector3 pos = enemies[i].gameObject.transform.position;
                pos.x = centerX;
                pos.z = centerZ + (i * offsetZ);
                enemies[i].gameObject.transform.position = pos;
            }
                    
            battleState = BattleState.ONGOING;
        }

        void Update()
        {
            Inputs();
            if (inputPause)
            {
                IsPaused = !IsPaused;
                //Fire event to pause all execution
            }


            /* currently hardcoded into States.cs statemachine
            if (battleType == BattleType.WAIT)
            {
                if (turnQueue.Count > 0)
                {
                    //remove first in queue and begin decision state
                }
            }
            */

            if (battleState == BattleState.ONGOING)
            {
                battleState = CheckBattleState();
            }
        }
        #endregion

        #region Helper Methods
        //turn based combat queue management
        public void AddToQueue(Character npc)
        {
            if (!turnQueue.Contains(npc))
                turnQueue.Add(npc);
        }

        public void RemoveFromQueue(Character npc)
        {
            if (turnQueue.Contains(npc))
                turnQueue.Remove(npc);
        }

        //checks to see if all players or all enemies are dead
        private BattleState CheckBattleState()
        {
            bool playersAlive = false;
            foreach (Player p in players)
            {
                if (!p.IsKO)
                    playersAlive = true;
            }

            bool enemiesAlive = false;
            foreach (Enemy e in enemies)
            {
                if (!e.IsKO)
                    enemiesAlive = true;
            }

            if (!playersAlive)
            {
                DoBattleLose();
                return BattleState.IDLE;
            }
            else if (!enemiesAlive)
            {
                DoBattleWin();
                return BattleState.IDLE;
            }
            else
                return BattleState.ONGOING;
        }

        //Get non-menu based input
        void Inputs()
        {
            inputPause = Input.GetButtonDown("Start");
        }

        void LockQueue()
        {
            QueueIsLocked = true;
        }

        void UnlockQueue()
        {
            QueueIsLocked = false;
        }

        public void DoBattleWin()
        {
            //calculate total experience and gold gained
            int expSum = 0;
            int goldSum = 0;


            //add dropped items to inventory
            List<Item> drops = new List<Item>();
            foreach (Enemy e in enemies)
            {
                expSum += e.Stats.experience;
                goldSum += e.Drops.dropTable.Gold;

                LootDrop drop = e.Drops.dropTable.GetDrop();
                if (drop != null)
                {
                    drops.Add(drop.item);
                    Debug.Log("Dropped: " + drop.item.Name);
                    GameManager.instance.playerInventory.Add(drop.item);
                }
            }
            GameManager.instance.playerInventory.Gold += goldSum;


            //give experience to alive players
            foreach (Player p in players)
            {
                if (!p.IsKO)
                {
                    p.GainExperience(expSum);
                }
            }

            victoryPanel.SetActive(true);
            victoryPanel.GetComponent<UI_LootWindow>().SetLootParams(expSum, goldSum, drops);
            victoryPanel.GetComponent<UI_LootWindow>().Open();
        }

        public void DoBattleLose()
        {
            //Debug.Log("You lose");
            //do gameover stuff
            gameOverPanel.SetActive(true);

        }

        public List<Enemy> GetAliveEnemies()
        {
            List<Enemy> aliveEnemies = new List<Enemy>();
            foreach (Enemy e in enemies)
            {
                if (!e.IsKO)
                {
                    aliveEnemies.Add(e);
                }
            }
            return aliveEnemies;
        }

        public List<Player> GetAlivePlayers()
        {
            List<Player> alivePlayers = new List<Player>();
            foreach (Player p in players)
            {
                if (!p.IsKO)
                {
                    alivePlayers.Add(p);
                }
            }
            return alivePlayers;
        }
        #endregion

        public void LoadWorldScene()
        {
            GameSceneManager.instance.LoadWorldScene();
        }

        #region Event Handling
        public void OnEnable()
        {
            EventManager.StartListening("LockQueue", queueLockListener);
            EventManager.StartListening("UnlockQueue", queueUnlockListener);
        }

        public void OnDisable()
        {
            EventManager.StopListening("LockQueue", queueLockListener);
            EventManager.StopListening("UnlockQueue", queueUnlockListener);
        }
        #endregion
    }
}
