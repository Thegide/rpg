﻿using UnityEngine;

namespace Maelstrom.RPG
{
    [CreateAssetMenu(menuName = "RPG/Actions/Attack")]
    public class Attack : Action
    {
        public override void DoAction(Character source, Character target)
        {
            parent = source; //assign the calling entity in the base class

            //for animations
            if (source.GetType() == typeof(Enemy))
            {
                source.StartCoroutine(((Enemy)source).Attack());  //calls EndTurn() - needs to move to state machine?
            }
            else if (source.GetType() == typeof(Player))
            {
                source.StartCoroutine(((Player)source).Attack());
            }
            else {
                Debug.LogError("Something went wrong in DoAction()");
            }

            DamageObject damage = CalcDamage(source, target);
            target.ApplyDamage(damage);
        }
    }
}