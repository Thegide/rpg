﻿#pragma warning disable CS0649 //supress warnings about main properties not being assigned

using System;
using UnityEngine;

namespace Maelstrom.RPG {

    public enum SpellType { HEALING, SUPPORT, ELEMENTAL, ENFEEBLE };

    //in theory, spell could derive from Action
    public abstract class Spell : Action, IComparable {

        #region Variables
       
        public SpellType spellType;
        public float CastTime;

        public Element element;
        public ParticleSystem visualEffect;
        #endregion

        #region Methods

        public void OnEnable()
        {
            m_actionType = ActionType.SPELL;
        }

        public override void DoAction(Character source, Character target)
        {
            CastSpell(source, target);
        }

        public abstract void CastSpell(Character source, Character target);

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            Spell spell = obj as Spell;

            if (this.spellType < spell.spellType)
            {
                return -1;
            }
            else if (this.spellType > spell.spellType)
            {
                return 1;
            }
            else
            {
                return this.Name.CompareTo(spell.Name);
            }

        }
        #endregion

    }
}