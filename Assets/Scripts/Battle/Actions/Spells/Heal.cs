﻿using UnityEngine;

namespace Maelstrom.RPG
{
    [CreateAssetMenu(menuName = "RPG/Spells/Heal")]
    public class Heal : Spell
    {
        public override void CastSpell(Character source, Character target)
        {
            parent = source; //assign the calling entity in the base class

            //for animations
            if (source.GetType() == typeof(Enemy))
            {
                source.StartCoroutine(((Enemy)source).Cast());  //calls EndTurn() - needs to move to state machine?
            }
            else if (source.GetType() == typeof(Player))
            {
                source.StartCoroutine(((Player)source).Cast());
            }
            else
            {
                Debug.LogError("Something went wrong in CastSpell()");

            }

            DamageObject damage = CalcDamage(source, target);
            target.ApplyDamage(damage);
        }
    }
}