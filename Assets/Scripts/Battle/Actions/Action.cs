﻿#pragma warning disable CS0649 //supress warnings about main properties not being assigned
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

namespace Maelstrom.RPG
{
    public enum ActionType { COMMAND, SPELL, ITEM }

    public abstract class Action : ScriptableObject
    {
        #region Variables
        private Sprite m_sprite;
        [SerializeField]
        private string m_name;
        protected ActionType m_actionType;
        [SerializeField]
        private TargetType m_targetType;
        
        public Sprite Icon { get { return m_sprite; } }
        public string Name { get { return m_name; } }
        public ActionType ActionType { get { return m_actionType; } } 
        public TargetType TargetSet { get { return m_targetType; } }
        public float CooldownTime; // to remove?
        public float Power;
        public bool CanHaveMultipleTargets;

        protected Character parent; //reference to calling entity
        #endregion

        #region Methods

        public abstract void DoAction(Character source, Character target);

        protected DamageObject CalcDamage(Character source, Character target)
        {
            if (!source || !target)
            {
                Debug.LogError("Cannot calculate damage if source or target is null");
                return null;
            }

            DamageObject damage = new DamageObject(this, source, target);
            return damage;

        }
        #endregion
    }

}