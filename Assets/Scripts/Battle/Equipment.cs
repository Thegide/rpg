﻿using System;
using UnityEngine;

namespace Maelstrom.RPG
{
    [Serializable]
    [CreateAssetMenu(menuName = "RPG/Equipment")]
    public class Equipment : ScriptableObject
    {
        public Weapon weapon;
        public Armor armor;
        public Accessory accessory;
    }
}