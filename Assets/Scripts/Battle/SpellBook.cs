﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Maelstrom.RPG
{
    [CreateAssetMenu(menuName = "RPG/SpellBook")]
    [System.Serializable]
    public class SpellBook : ScriptableObject
    {
        #region Variables
        [SerializeField]
        private List<Spell> spells = new List<Spell>();
        public Spell[] Spells { get { return spells.ToArray(); } }
        public int Count { get { return spells.Count; } }
        #endregion

        #region Methods
        void OnEnable()
        {
            Sort();
        }

        public void Add(Spell spell)
        {
            if (!spells.Contains(spell))
            {
                spells.Add(spell);
            }
        }

        public void Remove(Spell spell)
        {
            if (spells.Contains(spell))
            {
                spells.Remove(spell);
            }
        }

        public Spell FindSpell(string name)
        {
            Spell s = spells.Find(spell => spell.Name == name);
            return s;
        }

        private void Sort()
        {
            spells.Sort();
        }
        #endregion
    }
}