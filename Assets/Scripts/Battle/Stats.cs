﻿using System;
using UnityEngine;

namespace Maelstrom.RPG
{

    [Serializable]
    [CreateAssetMenu(menuName = "RPG/Entity")]
    public class Stats : ScriptableObject
    {
        public new string name;       
        public Sprite sprite;
        public int level;
        public int experience;
        public int maxHP;
        public int maxMP;
        
        [Header("Stats")]
        public int strength;
        public int vitality;
        public int intelligence;
        public int spirit;
        public float speed;

        [Range(0.0f, 1.0f)]
        public float evasion;

        //[HideInInspector]
        [Header("Secondary Stats")]
        [ReadOnly] public int atk;
        [ReadOnly] public int def, matk, mdef;
        public Element Element;

        [Header("Abilities")]
        public Action[] Actions;

        #region Methods

        //called at instance creation and start of play
        public void OnEnable()
        {
            atk = strength + level;
            def = vitality + level;
            matk = intelligence + level;
            mdef = spirit + level;
        }
       
        #endregion
    }  
}