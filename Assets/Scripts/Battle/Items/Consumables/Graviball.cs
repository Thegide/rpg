﻿using UnityEngine;

namespace Maelstrom.RPG
{
    [CreateAssetMenu(menuName = "RPG/Item/Graviball")]
    public class Graviball : Item
    {
        public override void Use(Character target)
        {
            if (target.IsKO)
                return;

            int damage = Mathf.RoundToInt(target.HP * 0.2f);
            target.HP -= damage;

            target.CreateDamageText(damage.ToString(), Color.red);
            Debug.Log(target.Name + " takes " + damage + " damage.");
        }
    }
}

