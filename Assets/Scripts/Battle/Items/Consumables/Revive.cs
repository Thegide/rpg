﻿using UnityEngine;

namespace Maelstrom.RPG
{
    [CreateAssetMenu(menuName = "RPG/Item/Revive")]
    public class Revive : Item
    {
        public override void Use(Character target)
        {
            if (!target.IsKO)
                return;

            int healAmount = Mathf.RoundToInt(0.2f * target.MaxHP);
            target.HP += healAmount;
            target.IsKO = false;

            target.CreateDamageText(healAmount.ToString(), Color.green);
            Debug.Log(target.Name + " recovers " + healAmount + " HP.");
        }
    }
}