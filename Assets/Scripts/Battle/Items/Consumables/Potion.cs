﻿using UnityEngine;

namespace Maelstrom.RPG
{
    [CreateAssetMenu(menuName = "RPG/Item/Potion")]
    public class Potion : Item
    {
        public int healAmount;

        public override void Use(Character target)
        {
            if (target.IsKO)
                return;

            int amount = Mathf.Min(healAmount, target.MaxHP - target.HP);
            //apply effect
            target.HP += amount;
                
            target.CreateDamageText(healAmount.ToString(), Color.green);
            Debug.Log(target.Name + " recovers " + amount + " HP.");
        }
    }
}