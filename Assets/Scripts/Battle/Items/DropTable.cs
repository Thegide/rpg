﻿using System;
using UnityEngine;

namespace Maelstrom.RPG
{

    [CreateAssetMenu(menuName = "RPG/DropTable")]
    public class DropTable : ScriptableObject
    {
        [Header("Drops")]
        public LootDropTable dropTable;

        public void OnEnable()
        {
            dropTable.ValidateTable();
        }
    }
}