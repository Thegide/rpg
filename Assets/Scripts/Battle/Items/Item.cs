﻿using System;
using UnityEngine;

namespace Maelstrom.RPG
{
    public class Item : ScriptableObject, IComparable
    {
    
        #region Variables   
        [Header("Item Properties")]
        [SerializeField]
        private string _name;
        [SerializeField]
        private Sprite icon;  //item icon in the inventory window
        [SerializeField]
        private int _stackSize;
        [SerializeField]
        private int _cost;

        [SerializeField]
        private TargetType m_targetType;
        public TargetType TargetSet { get { return m_targetType; } }

        public bool IsUnique; //single item per game (key item)
        public bool IsStackable; //single stack per game
        public bool DestroyOnUse; //consumable or reusable
        public bool CanUseInBattle;

        [Header("Item Description")]
        [SerializeField]
        private string _description;

        //Properties
        public string Name { get { return _name; } }        
        public int Cost { get { return _cost; } set { _cost = value; } }
        public int StackSize {  get { return _stackSize; } }
        public string Description { get { return _description; } }
        public Sprite Icon { get { return icon; } }
        #endregion

        #region Methods

        public int SellValue()
        {
            return _cost / 2;
        }

        public virtual void Use(Character target)
        {
            //do nothing - could implement as abstract?
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            Item item = obj as Item;

            return this.Name.CompareTo(item.Name);
        }
        
        #endregion
    }
}