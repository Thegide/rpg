﻿using UnityEngine;

namespace Maelstrom.RPG
{

    [CreateAssetMenu(menuName = "RPG/Item/Armors/Armor")]
    public class Armor : Item
    {
        public int defense = 0;
        public int magicDefense = 0;
    }
}
