﻿using UnityEngine;

namespace Maelstrom.RPG
{
    /// <summary>
    /// Item that can be picked by a LootDropTable.
    /// </summary>
    [System.Serializable]
    public class LootDrop : GenericLootDrop<Item> { }

    public abstract class GenericLootDrop<T>
    {
        public T item;

        // How many units the item takes - more units, higher chance of being picked
        [Range(0,256)]
        public float weight;  
        [ReadOnly]public float probability;

        // These values are assigned via LootDropTable script. They represent from which number to which number if selected, the item will be picked.
        [HideInInspector]
        public float probabilityRangeFrom;
        [HideInInspector]
        public float probabilityRangeTo;
    }
}