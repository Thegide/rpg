﻿using System.Collections.Generic;
using UnityEngine;

namespace Maelstrom.RPG
{
    /// <summary>
    /// Class serves for assigning and picking loot drop items.
    /// </summary>
    [System.Serializable]
    public class LootDropTable : GenericLootDropTable<LootDrop, Item> { }

    public abstract class GenericLootDropTable<T, U> where T : GenericLootDrop<U>
    {
        const int TotalWeight = 256;

        [SerializeField]
        private int m_gold = 0;
        public int Gold { get { return m_gold; } }

        // List where we'll assign the items.
        [SerializeField]
        public List<T> lootDropItems;

        /// <summary>
        /// Calculates the percentage and asigns the probabilities how many times
        /// the items can be picked. Function used also to validate data when tweaking numbers in editor.
        /// </summary>	
        public void ValidateTable()
        {

            // Prevent editor from "crying" when the item list is empty :)
            if (lootDropItems != null && lootDropItems.Count > 0)
            {

                float weightSum = 0f;

                // Sets the weight ranges of the selected items.
                foreach (T lootDrop in lootDropItems)
                {

                    lootDrop.probabilityRangeFrom = weightSum;
                    weightSum += lootDrop.weight;
                    lootDrop.probabilityRangeTo = weightSum;
                }

                // Calculate percentage of item drop select rate.
                foreach (T lootDropItem in lootDropItems)
                {
                    lootDropItem.probability = ((lootDropItem.weight) / TotalWeight) * 100;
                }

                if (weightSum > TotalWeight)
                    Debug.LogError("Combined weights of all items exceeds the maximum of " + TotalWeight);
            }            
        }

        /// <summary>
        /// Picks and returns the loot drop item based on it's probability.
        /// </summary>
        public T GetDrop()
        {

            float roll = Random.Range(0, TotalWeight);

            // Find an item whose range contains pickedNumber
            foreach (T lootDrop in lootDropItems)
            {
                // If the picked number matches the item's range, return item
                if (roll >= lootDrop.probabilityRangeFrom && roll < lootDrop.probabilityRangeTo)
                {
                    return lootDrop;
                }
            }

            return null;
        }

    }
}