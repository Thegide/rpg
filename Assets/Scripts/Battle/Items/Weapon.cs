﻿using UnityEngine;

namespace Maelstrom.RPG
{

    public enum WeaponType
    {
        Unarmed = 0,
        TwoHandSword = 1,
        TwoHandSpear = 2,
        TwoHandAxe = 3,
        Bow = 4, 
        Crossbow = 5,
        Staff = 6,
        Shield = 7,
        LSword = 8,
        RSword = 9,
        LMace = 10,
        RMace = 11,
        LDagger = 12,
        RDagger = 13,
        LKnife = 14,
        RKnife = 15,
        LPistol = 16,
        RPistol = 17,
        Rifle = 18,
        RSpear = 19,
        TwoHandClub = 20
    }

    [CreateAssetMenu(menuName = "RPG/Item/Weapons/Weapon")]
    public class Weapon : Item
    {
        public WeaponType weaponType = 0;

        public int attack = 0;
        public int magicAttack = 0;
    }
}