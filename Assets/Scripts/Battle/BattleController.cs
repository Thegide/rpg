﻿using UnityEngine;

namespace Maelstrom.RPG
{

    /*Finite State Machine for controlling NPC battle states
     *the battlecontroller object can be added to any NPC object to give them functionality of a 
     *state machine in the context of an in-game battle
     *its purpose is to decouple the functionality of battle from the NPC object
     */
    public class BattleController : MonoBehaviour
    {
        protected Character npc;
        public Character NPC { get { return npc; } }
        protected IState currentState;

        public float MaxTimeToNextAction, TimeToNextAction = 0;

        void Start()
        {
            MaxTimeToNextAction = 10; //ATB duration - currently hardcoded.  in the future will derive from action length
            TimeToNextAction = Random.Range(0, MaxTimeToNextAction); //random start ATB

            npc = GetComponent<Character>();
            ChangeState(new IdleState());
        }

        void Update()
        {
            currentState.Update(); //state machine execution
        }


        public void ChangeState(IState newstate)
        {
            if (currentState != null)
                currentState.Exit();

            currentState = newstate;
            currentState.Enter(this);
        }
    }
}