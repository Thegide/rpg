﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace Maelstrom.RPG
{
    
    public enum TargetType { SELF, ALLY, ENEMY, ANY, KO };

    //contains a variety of functions to return different sets of potential targets
    public class TargetSet
    {
        private List<Character> friendlyTargets;
        private List<Character> hostileTargets;

        //constructor
        public TargetSet (Character parent)
        {
            if (BattleManager.instance)
            {
                if (parent.GetType() == typeof(Player))  //may need to use object tags once helper NPCs are implemented
                {
                    friendlyTargets = BattleManager.instance.players.Cast<Character>().ToList();
                    hostileTargets = BattleManager.instance.enemies.Cast<Character>().ToList();
                }
                else if (parent.GetType() == typeof(Enemy))
                {
                    friendlyTargets = BattleManager.instance.enemies.Cast<Character>().ToList();
                    hostileTargets = BattleManager.instance.players.Cast<Character>().ToList();
                }
            }
            else
            {
                if (parent.GetType() == typeof(Player))
                {
                    //friendlyTargets = GameObject.FindGameObjectsWithTag("Player").ToList<NPC>();
                    //hostileTargets = GameObject.FindGameObjectsWithTag("Enemy").ToList<NPC>();
                }
                else if (parent.GetType() == typeof(Enemy))
                { 
                    //hostileTargets = GameObject.FindGameObjectsWithTag("Player").ToList<NPC>();
                    //friendlyTargets = GameObject.FindGameObjectsWithTag("Enemy").ToList<NPC>();
                }
            }
        }

        #region Main Methods
        //returns all alive players and enemies
        public List<Character> GetAllTargets()
        {
            List<Character> newList = new List<Character>();
            newList = friendlyTargets.Concat(hostileTargets).ToList();

            return newList;
        }

        public List<Character> GetAllTargets(bool IsAlive)
        {
            List<Character> allTargets = GetAllTargets();
            List<Character> newList = new List<Character>();

            foreach (Character npc in allTargets)
            {
                if (npc.IsKO != IsAlive)
                    newList.Add(npc);
            }

            return newList;
        }

        public List<Character> GetFriendlyTargets()
        {
            return friendlyTargets;
        }

        public List<Character> GetFriendlyTargets(bool IsAlive)
        {
            List<Character> newList = new List<Character>();

            foreach (Character npc in friendlyTargets)
            {
                if (npc.IsKO != IsAlive)
                     newList.Add(npc);
            }                
            
            return newList;            
        }

        public List<Character> GetHostileTargets()
        {
             return hostileTargets;
        }

        public List<Character> GetHostileTargets(bool IsAlive)
        {
            List<Character> newList = new List<Character>();

            foreach (Character npc in hostileTargets)
            {
                if (npc.IsKO != IsAlive)
                    newList.Add(npc);
            }

            return newList;
        }


        #endregion
    }
}