﻿using UnityEngine;

namespace Maelstrom.RPG
{

    public class DamageObject
    {

        #region Variables
        Character source, target;
        Action action;

        public int hpDamage = 0;
        public int mpDamage = 0;
        public bool Missed = false;
        #endregion

        //constructor
        public DamageObject(Action action, Character source, Character target)
        {
            this.source = source;
            this.target = target;
            this.action = action;

            getDamage();
        }

        #region Methods
        public DamageObject getDamage()
        {
            if (action.ActionType == ActionType.SPELL)
            {
                //Debug.Log("Calculating magical damage: " + action.GetType());
                hpDamage = CalcMagical();
            } 
            else
            {
                //Debug.Log("Calculating physical damage: " + action.GetType());
                hpDamage = CalcPhysical();
            }
            mpDamage = 0; //mp damaging attacks not yet implemented

            return this;
        }

        int CalcPhysical()
        {
            //Equation 1.  Initial base damage calculation
            int damage = (int)(action.Power * source.Level * Mathf.Pow(source.Atk, 1.75F) / 128) + 16;

            //Equation 2. Variable damage modifier = 10%
            damage = (int)(damage * Random.Range(0.9f, 1.1f));

            //Equation 3. Damage reduction
            int reduction = target.Level * target.Def / 128;
            //Debug.Log("DR: " + reduction);
            damage -= reduction;

            #region Damage Modifiers
            //Critical attack = 1.5x damage
            bool critical = Probability(0.1f); //hardcoded crit rate 10%
            if (critical)
            {
                damage = (int)Mathf.Round(damage * 1.5f);                
            }

            //Evasion calculation.            
            Missed = Probability(target.Evasion);
            if (Missed)
            {
                Debug.Log(target.Name + " dodges the attack!");
                return 0;
            }
            #endregion

            if (critical)
                Debug.Log("Critical attack! " + target.Name + " takes " + damage + " damage");
            else
                Debug.Log(target.Name + " takes " + damage + " damage");

            return damage;
        }

        int CalcMagical()
        {
            Spell spell = action as Spell;

            //Equation 1.  Initial base damage calculation
            int damage = (int)(spell.Power * source.Level * Mathf.Pow(source.MAtk, 1.75F) / 128) + 16; //+16 is the problem

            //Equation 2. Variable damage modifier = 10%
            damage = (int)(damage * Random.Range(0.9f, 1.1f));

            //Equation 3. Damage reduction
            int reduction = target.Level * target.MDef / 128;
            //Debug.Log("DR: " + reduction);
            damage -= reduction;

            if (spell.spellType == SpellType.HEALING)
            {
                damage = -damage;
            }

            return damage;
        }

        //simulates an event happening based on a probability rate
        //returns true if the event happen or false if the event did not happen
        public bool Probability (float rate)
        {
            if (Random.value <= rate)
                return true;
            else
                return false;
        }

    }
    #endregion
}
