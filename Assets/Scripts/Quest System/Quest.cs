﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Maelstrom.RPG
{
    public enum QuestState { inactive, active, success, fail }

    public class Quest : ScriptableObject
    {

        [SerializeField]
        string m_name;
        [SerializeField]
        string m_description;

        public string Name { get { return m_name; } }
        public string Description { get { return m_description; } }

        Dictionary<string, string> requirements;
        Dictionary<string, Task> tasks;

        public void SendMessage(string msg, int value)
        {
            Task task;
            if (tasks.TryGetValue(msg, out task))
            {
                task.currentProgress += value;
            }
        }

        public QuestState getProgress()
        {
            //    if (currentProgress < 0)
            //    {
            //        return QuestState.inactive;
            //    }

            //    else if (currentProgress == 99)
            //    {
            //        return QuestState.fail;
            //    }

            //    else if (currentProgress == 100)
            //    {
            //        return QuestState.success;
            //    }

            //    else 
            return QuestState.active;
        }

    }

    class Task : ScriptableObject
    {
        string description;
        public int currentProgress = -1;
        int targetProgress;

        
    }
}


